<h1 align="center">ls-micro</h1>

**复刻wujie微前端框架**
<br /> \
基于`webComponent`和`iframe`提供微前端能力，架构和编码基于[wujie](https://wujie-micro.github.io/doc/)微前端框架

### 目标

**实现mini-wujie在项目中的实际使用，支持`react`框架微应用互联互通**

---

> 只包含 `wujie-core`和 `wujie-react`部分能力，暂不支持其它框架接入

### 注意点

**用于个人使用和学习目的，生产不建议使用**

### 改动点

- 基于`wujie`微前端框架进行改造，修改参数名和小部分逻辑，源码添加注释，方便阅读
- 基于 `pnpm-workspace` + `turbo`架构
- 只提供 `react`微前端能力，即主子应用均为`react`，`example`目录下有示例
- `core`层使用`tsup`进行打包，享受`esbuild`和`swc`的编译速度
- `react组件`层使用`webpack`打包`umd`, `babel`打包 `esm`
