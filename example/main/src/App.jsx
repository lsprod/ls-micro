import { useState } from 'react';
import LSMicroReact from '@lgfe/fe-micro-react';

import './App.css';

function App() {
  const [count, setCount] = useState(0);

  return (
    <>
      <div>main</div>
      <div>
        <LSMicroReact
          width="100%"
          height="100%"
          name="project1"
          url="//localhost:8001/"
          plugins={[
            {
              jsBeforeLoaders: [
                {
                  callback(appWindow) {
                    appWindow.__REACT_DEVTOOLS_GLOBAL_HOOK__ = window.__REACT_DEVTOOLS_GLOBAL_HOOK__;
                  },
                },
              ],
            },
          ]}
        ></LSMicroReact>
        <LSMicroReact
          width="100%"
          height="100%"
          name="project2"
          url="//localhost:8002/"
          plugins={[
            {
              jsBeforeLoaders: [
                {
                  callback(appWindow) {
                    appWindow.__REACT_DEVTOOLS_GLOBAL_HOOK__ = window.__REACT_DEVTOOLS_GLOBAL_HOOK__;
                  },
                },
              ],
            },
          ]}
        ></LSMicroReact>
      </div>
    </>
  );
}

export default App;
