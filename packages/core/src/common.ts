/**
 * @file 公用微前端函数
 */

import type { LSMICRO } from './sandbox';
import type { CacheOptions, SandboxCache } from './types';

export type appAddEventListenerOptions = AddEventListenerOptions & { targetWindow?: Window };

/** 需要单独处理的window属性 */
export const windowProxyProperties = ['getComputedStyle', 'visualViewport', 'matchMedia', 'DOMParser'];

/** window白名单 */
export const windowRegWhiteList = [
  /animationFrame$/i,
  /resizeObserver$|mutationObserver$|intersectionObserver$/i,
  /height$|width$|left$/i,
  /^screen/i,
  /X$|Y$/,
];

/** 子应用window.onXXX需要挂载到iframe沙箱上的事件 */
export const appWindowOnEvent = ['onload', 'onbeforeunload', 'onunload'];

/** 需要挂载到子应用iframe document上的事件 */
export const appDocumentAddEventListenerEvents = ['DOMContentLoaded', 'readystatechange'];

/** 子应用document上的事件(需要被排除) */
export const appDocumentOnEvents = ['onreadystatechange'];

/** 分类document上需要处理的属性，不同类型会进入不同的处理逻辑 */
export const documentProxyProperties = {
  // 降级场景下需要本地特殊处理的属性
  modifyLocalProperties: ['createElement', 'createTextNode', 'documentURI', 'URL', 'getElementsByTagName'],

  // 子应用需要手动修正的属性方法
  modifyProperties: [
    'createElement',
    'createTextNode',
    'documentURI',
    'URL',
    'getElementsByTagName',
    'getElementsByClassName',
    'getElementsByName',
    'getElementById',
    'querySelector',
    'querySelectorAll',
    'documentElement',
    'scrollingElement',
    'forms',
    'images',
    'links',
  ],

  // 需要从shadowRoot中获取的属性
  shadowProperties: [
    'activeElement',
    'childElementCount',
    'children',
    'firstElementChild',
    'firstChild',
    'fullscreenElement',
    'lastElementChild',
    'pictureInPictureElement',
    'pointerLockElement',
    'styleSheets',
  ],

  // 需要从shadowRoot中获取的方法
  shadowMethods: [
    'append',
    'contains',
    'getSelection',
    'elementFromPoint',
    'elementsFromPoint',
    'getAnimations',
    'replaceChildren',
  ],

  // 需要从主应用document中获取的属性
  documentProperties: [
    'characterSet',
    'compatMode',
    'contentType',
    'designMode',
    'dir',
    'doctype',
    'embeds',
    'fullscreenEnabled',
    'hidden',
    'implementation',
    'lastModified',
    'pictureInPictureEnabled',
    'plugins',
    'readyState',
    'referrer',
    'visibilityState',
    'fonts',
  ],

  // 需要从主应用document中获取的方法
  documentMethods: [
    'execCommand',
    'caretPositionFromPoint',
    'createRange',
    'exitFullscreen',
    'exitPictureInPicture',
    'getElementsByTagNameNS',
    'hasFocus',
    'prepend',
  ],

  // 需要从主应用document中获取的事件
  documentEvents: [
    'onpointerlockchange',
    'onpointerlockerror',
    'onbeforecopy',
    'onbeforecut',
    'onbeforepaste',
    'onfreeze',
    'onresume',
    'onsearch',
    'onfullscreenchange',
    'onfullscreenerror',
    'onsecuritypolicyviolation',
    'onvisibilitychange',
  ],

  // 无需修改原型的属性
  ownerProperties: ['head', 'body'],
};

/** 需要挂载到主应用document上的事件 */
export const mainDocumentAddEventListenerEvents = [
  'fullscreenchange',
  'fullscreenerror',
  'selectionchange',
  'visibilitychange',
  'wheel',
  'keydown',
  'keypress',
  'keyup',
];

/** 需要同时挂载到主应用document和shadow上的事件（互斥） */
export const mainAndAppAddEventListenerEvents = ['gotpointercapture', 'lostpointercapture'];

/** 子应用window监听需要挂载到iframe沙箱上的事件 */
export const appWindowAddEventListenerEvents = [
  'hashchange',
  'popstate',
  'DOMContentLoaded',
  'load',
  'beforeunload',
  'unload',
  'message',
  'error',
  'unhandledrejection',
];

// 全部ls-micro实例和配置存储map
export const idToSandboxCacheMap = window.__IS_IN_LS_MICRO
  ? window.__LS_MICRO.inject.idToSandboxMap
  : new Map<String, SandboxCache>();

/** 通过id获取微前端实例 */
export function getMicroById(id: string) {
  return idToSandboxCacheMap.get(id)?.micro;
}

/** 通过id删除micro cache */
export function deleteMicroById(id: string) {
  const microCache = idToSandboxCacheMap.get(id);
  // todo这个看不懂为啥这么整，讲道理一个map为何要重复赋值？
  if (microCache?.options) idToSandboxCacheMap.set(id, { options: microCache.options });
  idToSandboxCacheMap.delete(id);
}

/** 原生的querySelector */
export const rawDocumentQuerySelector = window.__IS_IN_LS_MICRO
  ? window.__LS_MICRO_RAW_DOCUMENT_QUERY_SELECTOR__
  : Document.prototype.querySelector;

// 存储原生dom方法，因为子应用的document.prototype被改写了
export const rawElementAppendChild = HTMLElement.prototype.appendChild;
export const rawElementRemoveChild = HTMLElement.prototype.removeChild;
export const rawElementContains = HTMLElement.prototype.contains;
export const rawHeadInsertBefore = HTMLHeadElement.prototype.insertBefore;
export const rawBodyInsertBefore = HTMLBodyElement.prototype.insertBefore;
export const rawAddEventListener = Node.prototype.addEventListener;
export const rawRemoveEventListener = Node.prototype.removeEventListener;
export const rawWindowAddEventListener = window.addEventListener;
export const rawWindowRemoveEventListener = window.removeEventListener;
export const rawAppendChild = Node.prototype.appendChild;

/** 相对路径问题元素的tag和attr的map */
export const relativeElementTagAttrMap = {
  IMG: 'src',
  A: 'href',
  SOURCE: 'src',
};

/** 添加沙箱缓存 */
export function addSandboxCacheWithOptions(id: string, options: CacheOptions) {
  const microCache = idToSandboxCacheMap.get(id);
  if (microCache) {
    idToSandboxCacheMap.set(id, { ...microCache, options });
  } else {
    idToSandboxCacheMap.set(id, { options });
  }
}

/** 通过micro属性，添加沙箱缓存 */
export function addSandboxCacheWithSandbox(id: string, sandbox: LSMICRO) {
  const microCache = idToSandboxCacheMap.get(id);
  if (microCache) idToSandboxCacheMap.set(id, { ...microCache, micro: sandbox });
  else idToSandboxCacheMap.set(id, { micro: sandbox });
}

/** 获取沙箱缓存options */
export function getOptionsById(id: String): CacheOptions | null {
  return idToSandboxCacheMap.get(id)?.options || null;
}
