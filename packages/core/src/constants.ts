/**
 * @file 全局常量
 */
export const LS_MICRO_APP = 'ls-micro-app';

export const LS_MICRO_ALL_EVENTS = '__ls-micro-all-events';

export const LS_MICRO_TIPS_STOP_MAIN = '此报错可忽略，iframe主动终止子应用代码在主应用的使用';

export const LS_MICRO_TIPS_NOT_SUPPORT = '当前浏览器不支持ls-micro，将采用iframe加载子应用';

export const LS_MICRO_TIPS_REPEAT_RENDER = '组件短时间重复渲染了两次，可能存在性能问题请检查代码';
export const LS_MICRO_TIPS_ERROR_REQUESTED = '脚本请求发生错误';
export const LS_MICRO_TIPS_HTML_ERROR_REQUESTED = 'html请求发生错误';

export const LS_MICRO_TIPS_NO_URL = 'url参数为空';
export const LS_MICRO_TIPS_NO_FETCH = '当前浏览器不支持fetch,需自行polyfill';
export const LS_MICRO_TIPS_CSS_ERROR_REQUESTED = '样式请求出现错误';
export const LS_MICRO_TIPS_SCRIPT_ERROR_REQUESTED = '脚本请求出现错误';
export const LS_MICRO_TIPS_NO_SCRIPT = '目标Script尚未准备好或已经被移除';
export const LS_MICRO_TIPS_RELOAD_DISABLED = '子应用调用reload无法生效';
export const LS_MICRO_TIPS_GET_ELEMENT_BY_ID =
  '不支持document.getElementById()传入特殊字符，请参考document.querySelector文档';

export const LOADING_DATA_FLAG = 'loading_data_flag';
export const CONTAINER_POSITION_DATA_FLAG = 'data-container-position-flag';
export const CONTAINER_OVERFLOW_DATA_FLAG = 'data-container-overflow-flag';

export const LS_MICRO_DATA_FLAG = 'ls_micro_data_flag';
export const LS_MICRO_DATA_ATTACH_CSS_FLAG = 'ls-micro-attach-css-flag';

export const LS_MICRO_APP_ID = 'ls_micro_app_id';
export const LS_MICRO_IFRAME_CLASS = 'ls_iframe';
export const LS_MICRO_SCRIPT_ID = 'ls_micro_script_id';

export const LS_MICRO_SHADE_STYLE =
  'position: fixed; z-index: 2147483647; visibility: hidden; inset: 0px; backface-visibility: hidden;';
export const LS_MICRO_LOADING_STYLE =
  'position: absolute; width: 100%; height: 100%; display: flex; justify-content: center; align-items: center; z-index:1;';

export const LS_MICRO_LOADING_SVG = `<svg xmlns="http://www.w3.org/2000/svg" width="24px" height="30px" viewBox="0 0 24 30">
<rect x="0" y="13" width="4" height="5" fill="#909090">
  <animate attributeName="height" attributeType="XML" values="5;21;5" begin="0s" dur="0.6s" repeatCount="indefinite"></animate>
  <animate attributeName="y" attributeType="XML" values="13; 5; 13" begin="0s" dur="0.6s" repeatCount="indefinite"></animate>
</rect>
<rect x="10" y="13" width="4" height="5" fill="#909090">
  <animate attributeName="height" attributeType="XML" values="5;21;5" begin="0.15s" dur="0.6s" repeatCount="indefinite"></animate>
  <animate attributeName="y" attributeType="XML" values="13; 5; 13" begin="0.15s" dur="0.6s" repeatCount="indefinite"></animate>
</rect>
<rect x="20" y="13" width="4" height="5" fill="#909090">
  <animate attributeName="height" attributeType="XML" values="5;21;5" begin="0.3s" dur="0.6s" repeatCount="indefinite"></animate>
  <animate attributeName="y" attributeType="XML" values="13; 5; 13" begin="0.3s" dur="0.6s" repeatCount="indefinite"></animate>
</rect>
</svg>`;
