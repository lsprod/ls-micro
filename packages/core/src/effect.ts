/**
 * @file 副作用相关逻辑
 */

import {
  getMicroById,
  rawAddEventListener,
  rawAppendChild,
  rawBodyInsertBefore,
  rawDocumentQuerySelector,
  rawElementContains,
  rawElementRemoveChild,
  rawHeadInsertBefore,
  rawRemoveEventListener,
} from './common';
import {
  LS_MICRO_DATA_FLAG,
  LS_MICRO_SCRIPT_ID,
  LS_MICRO_TIPS_NO_SCRIPT,
  LS_MICRO_TIPS_REPEAT_RENDER,
} from './constants';
import { getExternalScripts, getExternalStyleSheets } from './entry';
import { insertScriptToIframe, patchElementEffect } from './iframe';
import { getCssLoader, getEffectLoaders, isMatchUrl } from './plugin';
import type { LSMICRO } from './sandbox';
import { getPatchStyleElements } from './shadow';
import type { ScriptObject } from './template';
import { parseTagAttributes } from './template';
import {
  execHooks,
  getCurUrl,
  getTagFromScript,
  isFunction,
  isHijackingTag,
  isScriptElement,
  log,
  nextTick,
  setAttrsToElement,
  setTagToScript,
} from './utils';

/**
 * 记录head和body的事件，等重新渲染复用head和body时需要清空事件
 */
export function patchEventListener(element: HTMLHeadElement | HTMLBodyElement) {
  const listenerMap = new Map<string, EventListenerOrEventListenerObject[]>();
  // 把listeners都存入到_cacheListeners中
  (element as any)._cacheListeners = listenerMap;

  element.addEventListener = (
    type: string,
    listener: EventListenerOrEventListenerObject,
    options?: boolean | AddEventListenerOptions,
  ) => {
    const listeners = listenerMap.get(type) || [];
    listenerMap.set(type, [...listeners, listener]);
    return rawAddEventListener.call(element, type, listener, options);
  };

  element.removeEventListener = (
    type: string,
    listener: EventListenerOrEventListenerObject,
    options?: boolean | AddEventListenerOptions,
  ) => {
    const typeListeners = listenerMap.get(type);
    const index = typeListeners?.indexOf(listener);
    if (typeListeners?.length && index !== -1) {
      typeListeners.splice(index!, 1);
    }
    return rawRemoveEventListener.call(element, type, listener, options);
  };
}

/**
 * 清空head和body的绑定的事件
 */
export function removeEventListener(element: HTMLHeadElement | HTMLBodyElement) {
  const listenerMap = (element as any)._cacheListeners;
  [...listenerMap.entries()].forEach(([type, listeners]) => {
    listeners.forEach((listener: any) => rawRemoveEventListener.call(element, type, listener));
  });
}

/**
 * 在渲染中修补head和body
 * 拦截appendChild和insertBefore
 */
export function patchRenderEffect(render: Document | ShadowRoot, id: string, degrade: boolean) {
  // 降级场景dom渲染在iframe中，iframe移动后事件自动销毁，不需要记录
  if (!degrade) {
    // @ts-ignore
    patchEventListener(render.head);
    // @ts-ignore
    patchEventListener(render.body as HTMLBodyElement);
  }

  render.head.appendChild = rewriteAppendOrInsertChild({
    rawDOMAppendOrInsertBefore: rawAppendChild,
    // wujieId: id,
    microId: id,
  }) as typeof rawAppendChild;
  render.head.insertBefore = rewriteAppendOrInsertChild({
    rawDOMAppendOrInsertBefore: rawHeadInsertBefore as any,
    microId: id,
  }) as typeof rawHeadInsertBefore;

  render.head.removeChild = rewriteRemoveChild({
    rawElementRemoveChild: rawElementRemoveChild.bind(render.head),
    microId: id,
  }) as typeof rawElementRemoveChild;
  render.head.contains = rewriteContains({
    rawElementContains: rawElementContains.bind(render.head),
    microId: id,
  }) as typeof rawElementContains;
  render.contains = rewriteContains({
    rawElementContains: rawElementContains.bind(render),
    microId: id,
  }) as typeof rawElementContains;
  render.body.appendChild = rewriteAppendOrInsertChild({
    rawDOMAppendOrInsertBefore: rawAppendChild,
    microId: id,
  }) as typeof rawAppendChild;
  render.body.insertBefore = rewriteAppendOrInsertChild({
    rawDOMAppendOrInsertBefore: rawBodyInsertBefore as any,
    microId: id,
  }) as typeof rawBodyInsertBefore;
}

/**
 * 样式元素的css变量处理，每个stylesheetElement单独节流
 */
function handleStylesheetElementPatch(stylesheetElement: HTMLStyleElement & { _patcher?: any }, sandbox: LSMICRO) {
  if (!stylesheetElement.innerHTML || sandbox.degrade) return;
  const patcher = () => {
    const [hostStyleSheetElement, fontStyleSheetElement] = getPatchStyleElements([stylesheetElement.sheet!]);
    if (hostStyleSheetElement) {
      sandbox.shadowRoot!.head.appendChild(hostStyleSheetElement);
    }
    if (fontStyleSheetElement) {
      sandbox.shadowRoot!.host.appendChild(fontStyleSheetElement);
    }
    stylesheetElement._patcher = undefined;
  };
  if (stylesheetElement._patcher) {
    clearTimeout(stylesheetElement._patcher);
  }
  stylesheetElement._patcher = setTimeout(patcher, 50);
}

function patchCustomEvent(
  e: CustomEvent,
  elementGetter: () => HTMLScriptElement | HTMLLinkElement | null,
): CustomEvent {
  Object.defineProperties(e, {
    srcElement: {
      get: elementGetter,
    },
    target: {
      get: elementGetter,
    },
  });

  return e;
}

/**
 * 手动触发事件回调
 */
function manualInvokeElementEvent(element: HTMLLinkElement | HTMLScriptElement, event: string): void {
  const customEvent = new CustomEvent(event);
  const patchedEvent = patchCustomEvent(customEvent, () => element);
  // @ts-ignore
  if (isFunction(element[`on${event}`])) {
    // @ts-ignore
    element[`on${event}`](patchedEvent);
  } else {
    element.dispatchEvent(patchedEvent);
  }
}

/**
 * 劫持处理样式元素的属性
 */
// eslint-disable-next-line max-params
function patchStylesheetElement(
  stylesheetElement: HTMLStyleElement & { _hasPatchStyle?: boolean },
  cssLoader: (code: string, url: string, base: string) => string,
  sandbox: LSMICRO,
  curUrl: string,
) {
  if (stylesheetElement._hasPatchStyle) return;
  const innerHTMLDesc = Object.getOwnPropertyDescriptor(Element.prototype, 'innerHTML');
  const innerTextDesc = Object.getOwnPropertyDescriptor(HTMLElement.prototype, 'innerText');
  const textContentDesc = Object.getOwnPropertyDescriptor(Node.prototype, 'textContent');
  const RawInsertRule = stylesheetElement.sheet?.insertRule;
  // 这个地方将cssRule加到innerHTML中去，防止子应用切换之后丢失
  function patchSheetInsertRule() {
    if (!RawInsertRule) return;
    stylesheetElement.sheet.insertRule = (rule: string, index?: number): number => {
      innerHTMLDesc ? (stylesheetElement.innerHTML += rule) : (stylesheetElement.innerText += rule);
      return RawInsertRule.call(stylesheetElement.sheet, rule, index);
    };
  }
  patchSheetInsertRule();

  if (innerHTMLDesc) {
    Object.defineProperties(stylesheetElement, {
      innerHTML: {
        get: function () {
          return innerHTMLDesc.get!.call(stylesheetElement);
        },
        set: function (code: string) {
          innerHTMLDesc.set!.call(stylesheetElement, cssLoader(code, '', curUrl));
          nextTick(() => handleStylesheetElementPatch(this, sandbox));
        },
      },
    });
  }

  Object.defineProperties(stylesheetElement, {
    innerText: {
      get: function () {
        return innerTextDesc!.get!.call(stylesheetElement);
      },
      set: function (code: string) {
        innerTextDesc!.set!.call(stylesheetElement, cssLoader(code, '', curUrl));
        nextTick(() => handleStylesheetElementPatch(this, sandbox));
      },
    },
    textContent: {
      get: function () {
        return textContentDesc!.get!.call(stylesheetElement);
      },
      set: function (code: string) {
        textContentDesc!.set!.call(stylesheetElement, cssLoader(code, '', curUrl));
        nextTick(() => handleStylesheetElementPatch(this, sandbox));
      },
    },
    appendChild: {
      value: function (node: Node): Node {
        nextTick(() => handleStylesheetElementPatch(this, sandbox));
        if (node.nodeType === Node.TEXT_NODE) {
          const res = rawAppendChild.call(
            stylesheetElement,
            stylesheetElement.ownerDocument.createTextNode(cssLoader(node.textContent!, '', curUrl)),
          );
          // 当appendChild之后，样式元素的sheet对象发生改变，要重新patch
          patchSheetInsertRule();
          return res;
        } else return rawAppendChild(node);
      },
    },
    _hasPatchStyle: { get: () => true },
  });
}

let dynamicScriptExecStack = Promise.resolve();
/**
 * 重写appendChild 或 insertChild
 */
export function rewriteAppendOrInsertChild(opts: {
  rawDOMAppendOrInsertBefore: <T extends Node>(newChild: T, refChild?: Node | null) => T;
  microId: string;
}) {
  return function appendChildOrInsertBefore<T extends Node>(
    this: HTMLHeadElement | HTMLBodyElement,
    newChild: T,
    refChild?: Node | null,
  ) {
    let element = newChild as any;
    const { rawDOMAppendOrInsertBefore, microId } = opts;
    const sandbox = getMicroById(microId);

    const { styleSheetElements, replace, fetch, plugins, iframe, lifecycle, proxyLocation, fiber } = sandbox!;

    if (!isHijackingTag(element.tagName) || !microId) {
      const res = rawDOMAppendOrInsertBefore.call(this, element, refChild) as T;
      patchElementEffect(element, iframe!.contentWindow!);
      execHooks(plugins!, 'appendOrInsertElementHook', element, iframe!.contentWindow);
      return res;
    }

    const iframeDocument = iframe!.contentDocument;
    const curUrl = getCurUrl(proxyLocation!);

    if (element.tagName) {
      switch (element.tagName.toLowerCase()) {
        case 'LINK': {
          const { href, rel, type } = element as HTMLLinkElement;
          const styleFlag = rel === 'stylesheet' || type === 'text/css' || href.endsWith('.css');
          // 非stylesheet不做处理
          if (!styleFlag) {
            const res = rawDOMAppendOrInsertBefore.call(this, element, refChild);
            execHooks(plugins!, 'appendOrInsertElementHook', element, iframe!.contentWindow);
            return res;
          }
          // 排除css
          if (href && !isMatchUrl(href, getEffectLoaders('cssExcludes', plugins!))) {
            getExternalStyleSheets(
              [{ src: href, ignore: isMatchUrl(href, getEffectLoaders('cssIgnores', plugins!)) }],
              fetch,
              lifecycle!.loadError,
            ).forEach(({ src, ignore, contentPromise }) =>
              contentPromise.then(
                (content) => {
                  // 处理 ignore 样式
                  // 获取标签上的属性key value对象
                  const rawAttrs = parseTagAttributes(element.outerHTML);
                  if (ignore && src) {
                    // 忽略的元素应该直接把对应元素插入，而不是用新的 link 标签进行替代插入，保证 element 的上下文正常
                    rawDOMAppendOrInsertBefore.call(this, element, refChild);
                  } else {
                    // 记录js插入样式，子应用重新激活时恢复
                    const stylesheetElement = iframeDocument!.createElement('style');
                    // 处理css-loader插件
                    const cssLoader = getCssLoader({ plugins: plugins!, replace: replace! });
                    stylesheetElement.innerHTML = cssLoader(content, src, curUrl);
                    styleSheetElements!.push(stylesheetElement);
                    // 将link标签属性赋值给style元素
                    setAttrsToElement(stylesheetElement, rawAttrs);
                    rawDOMAppendOrInsertBefore.call(this, stylesheetElement, refChild);
                    // 处理样式补丁
                    handleStylesheetElementPatch(stylesheetElement, sandbox!);
                    manualInvokeElementEvent(element, 'load');
                  }
                },
                () => {
                  manualInvokeElementEvent(element, 'error');
                  element = null;
                },
              ),
            );
          }
          const comment = iframeDocument!.createComment(`dynamic link ${href} replaced by ls-micro`);
          return rawDOMAppendOrInsertBefore.call(this, comment, refChild);
        }
        case 'STYLE': {
          const stylesheetElement: HTMLStyleElement = newChild as any;
          styleSheetElements!.push(stylesheetElement);
          const content = stylesheetElement.innerHTML;
          const cssLoader = getCssLoader({ plugins: plugins!, replace: replace! });
          content && (stylesheetElement.innerHTML = cssLoader(content, '', curUrl));
          const res = rawDOMAppendOrInsertBefore.call(this, element, refChild);
          // 处理样式补丁(太难了，看不懂- -)
          patchStylesheetElement(stylesheetElement, cssLoader, sandbox!, curUrl);
          handleStylesheetElementPatch(stylesheetElement, sandbox!);
          execHooks(plugins!, 'appendOrInsertElementHook', element, iframe!.contentWindow);
          return res;
        }
        case 'SCRIPT': {
          setTagToScript(element);
          const { src, text, type, crossOrigin } = element as HTMLScriptElement;

          // 排除js
          if (src && !isMatchUrl(src, getEffectLoaders('jsExcludes', plugins!))) {
            const execScript = (scriptResult: ScriptObject) => {
              // 假如子应用被连续渲染两次，两次渲染会导致处理流程的交叉污染
              if (sandbox!.iframe === null) return log(LS_MICRO_TIPS_REPEAT_RENDER, 'warn');
              const onload = () => {
                manualInvokeElementEvent(element, 'load');
                element = null;
              };
              insertScriptToIframe({ ...scriptResult, onload }, sandbox!.iframe!.contentWindow!, element);
            };
            const scriptOptions: ScriptObject = {
              src,
              module: type === 'module',
              crossorigin: crossOrigin !== null,
              crossoriginType: crossOrigin || ('' as any),
              ignore: isMatchUrl(src, getEffectLoaders('jsIgnores', plugins!)),
              attrs: parseTagAttributes(element.outerHTML),
            };
            getExternalScripts([scriptOptions], fetch, lifecycle!.loadError, fiber!).forEach((scriptResult) => {
              dynamicScriptExecStack = dynamicScriptExecStack.then(() =>
                scriptResult.contentPromise.then(
                  (content) => {
                    if (sandbox!.execQueue === null) return log(LS_MICRO_TIPS_REPEAT_RENDER, 'warn');
                    const execQueueLength = sandbox!.execQueue?.length;
                    // eslint-disable-next-line max-nested-callbacks
                    sandbox!.execQueue!.push(() =>
                      fiber
                        ? // eslint-disable-next-line max-nested-callbacks
                          requestIdleCallback(() => {
                            execScript({ ...scriptResult, content });
                          })
                        : execScript({ ...scriptResult, content }),
                    );
                    // 同步脚本如果都执行完了，需要手动触发执行
                    if (!execQueueLength) sandbox!.execQueue!.shift()!();
                  },
                  () => {
                    manualInvokeElementEvent(element, 'error');
                    element = null;
                  },
                ),
              );
            });
          } else {
            const execQueueLength = sandbox!.execQueue?.length;
            sandbox!.execQueue!.push(() =>
              fiber
                ? requestIdleCallback(() => {
                    insertScriptToIframe(
                      // @ts-ignore
                      { src: null, content: text, attrs: parseTagAttributes(element.outerHTML) },
                      sandbox!.iframe!.contentWindow,
                      element,
                    );
                  })
                : insertScriptToIframe(
                    // @ts-ignore
                    { src: null, content: text, attrs: parseTagAttributes(element.outerHTML) },
                    sandbox!.iframe!.contentWindow,
                    element,
                  ),
            );
            if (!execQueueLength) sandbox!.execQueue!.shift()!();
          }
          // inline script never trigger the onload and onerror event
          const comment = iframeDocument!.createComment(`dynamic script ${src} replaced by ls-micro`);
          return rawDOMAppendOrInsertBefore.call(this, comment, refChild);
        }
        // 修正子应用内部iframe的window.parent指向
        case 'IFRAME': {
          // 嵌套的子应用的js-iframe需要插入子应用的js-iframe内部
          if (element.getAttribute(LS_MICRO_DATA_FLAG) === '') {
            return rawAppendChild.call(rawDocumentQuerySelector.call(this.ownerDocument, 'html'), element);
          }
          const res = rawDOMAppendOrInsertBefore.call(this, element, refChild);
          execHooks(plugins!, 'appendOrInsertElementHook', element, iframe!.contentWindow);
          return res;
        }
      }
    }
  };
}

function findScriptElementFromIframe(rawElement: HTMLScriptElement, microId: string) {
  const tag = getTagFromScript(rawElement);
  const sandbox = getMicroById(microId);
  const { iframe } = sandbox!;
  const targetScript = iframe!.contentWindow!.__LS_MICRO_RAW_DOCUMENT_HEAD__.querySelector(
    `script[${LS_MICRO_SCRIPT_ID}='${tag}']`,
  );
  if (targetScript === null) {
    log(LS_MICRO_TIPS_NO_SCRIPT + `<script ${LS_MICRO_SCRIPT_ID}='${tag}'/>`, 'warn');
  }
  return { targetScript, iframe };
}

/**
 * 重写removeChild
 */
function rewriteRemoveChild(opts: { rawElementRemoveChild: <T extends Node>(child: T) => T; microId: string }) {
  return function removeChild(child: Node) {
    const element = child as HTMLElement;
    const { rawElementRemoveChild, microId } = opts;
    if (element && isScriptElement(element)) {
      const { targetScript, iframe } = findScriptElementFromIframe(element as HTMLScriptElement, microId);
      if (targetScript !== null) {
        return iframe!.contentWindow!.__LS_MICRO_RAW_DOCUMENT_HEAD__.removeChild(targetScript);
      }
      return null;
    }
    return rawElementRemoveChild(element);
  };
}

/**
 * 重写contains
 */
function rewriteContains(opts: { rawElementContains: (other: Node | null) => boolean; microId: string }) {
  return function contains(other: Node | null) {
    const element = other as HTMLElement;
    const { rawElementContains, microId } = opts;
    if (element && isScriptElement(element)) {
      const { targetScript } = findScriptElementFromIframe(element as HTMLScriptElement, microId);
      return targetScript !== null;
    }
    return rawElementContains(element);
  };
}
