/**
 * @file 提供各类型文件导入能力逻辑
 */

import {
  LS_MICRO_TIPS_CSS_ERROR_REQUESTED,
  LS_MICRO_TIPS_HTML_ERROR_REQUESTED,
  LS_MICRO_TIPS_SCRIPT_ERROR_REQUESTED,
} from './constants';
import { getEffectLoaders, isMatchUrl } from './plugin';
import type { LSMICRO, loadErrorHandler } from './sandbox';
import {
  genLinkReplaceSymbol,
  getInlineStyleReplaceSymbol,
  processTpl,
  type ScriptBaseObject,
  type ScriptObject,
  type StyleObject,
} from './template';
import type { Plugin } from './types';
import { compose, defaultGetPublicPath, getCurUrl, getInlineCode, log } from './utils';

export type ScriptResultList = (ScriptBaseObject & { contentPromise: Promise<string> })[];
export type StyleResultList = { src: string; contentPromise: Promise<string>; ignore?: boolean }[];

interface htmlParseResult {
  template: string;

  assetPublicPath: string;

  getExternalScripts: () => ScriptResultList;

  getExternalStyleSheets: () => StyleResultList;
}

type ImportEntryOpts = {
  fetch?: typeof window.fetch;
  fiber?: boolean;
  plugins?: Array<Plugin>;
  loadError?: loadErrorHandler;
};

const styleCache = {};
const scriptCache = {};
const embedHTMLCache: any = {};

const defaultFetch = window.fetch.bind(window);

const isInlineCode = (code: string) => code.startsWith('<');

function defaultGetTemplate(tpl: any) {
  return tpl;
}

// eslint-disable-next-line max-params
function fetchAssets(
  src: string,
  cache: any,
  fetch: (input: RequestInfo, init?: RequestInit) => Promise<Response>,
  cssFlag?: boolean,
  loadError?: loadErrorHandler,
) {
  // eslint-disable-next-line no-return-assign
  return (
    cache[src] ||
    (cache[src] = fetch(src)
      .then((response) => {
        // usually browser treats 4xx and 5xx response of script loading as an error and will fire a script error event
        if (response.status >= 400) {
          cache[src] = null;
          if (cssFlag) {
            log(`${LS_MICRO_TIPS_CSS_ERROR_REQUESTED}: ${JSON.stringify({ src, response })}`, 'error');
            loadError?.(src, new Error(LS_MICRO_TIPS_CSS_ERROR_REQUESTED));
            return '';
          } else {
            log(`${LS_MICRO_TIPS_SCRIPT_ERROR_REQUESTED}: ${JSON.stringify({ src, response })}`, 'error');
            loadError?.(src, new Error(LS_MICRO_TIPS_SCRIPT_ERROR_REQUESTED));
            throw new Error(LS_MICRO_TIPS_SCRIPT_ERROR_REQUESTED);
          }
        }
        return response.text();
      })
      .catch((error) => {
        cache[src] = null;
        if (cssFlag) {
          log(`${LS_MICRO_TIPS_CSS_ERROR_REQUESTED}: ${src}`, 'error');
          loadError?.(src, error);
          return '';
        } else {
          log(`${LS_MICRO_TIPS_SCRIPT_ERROR_REQUESTED}: ${src}`, 'error');
          loadError?.(src, error);
          return '';
        }
      }))
  );
}

export function getExternalStyleSheets(
  styles: StyleObject[],
  fetch: (input: RequestInfo, init?: RequestInit) => Promise<Response> = defaultFetch,
  loadError: loadErrorHandler,
): StyleResultList {
  // @ts-ignore
  return styles.map(({ src, content, ignore }) => {
    // 内联
    if (content) {
      return { src: '', contentPromise: Promise.resolve(content) };
    } else if (isInlineCode(src!)) {
      // if it is inline style
      return { src: '', contentPromise: Promise.resolve(getInlineCode(src!)) };
    } else {
      // external styles
      return {
        src,
        ignore,
        contentPromise: ignore ? Promise.resolve('') : fetchAssets(src!, styleCache, fetch, true, loadError),
      };
    }
  });
}

// eslint-disable-next-line max-params
export function getExternalScripts(
  scripts: ScriptObject[],
  fetch: (input: RequestInfo, init?: RequestInit) => Promise<Response> = defaultFetch,
  loadError: loadErrorHandler,
  fiber: boolean,
): ScriptResultList {
  return scripts.map((script) => {
    const { src, async, defer, module, ignore } = script;
    let contentPromise = null;
    if ((async || defer) && src && !module) {
      // async 且非 es module
      contentPromise = new Promise((resolve, reject) => {
        fiber
          ? requestIdleCallback(() => fetchAssets(src, scriptCache, fetch, false, loadError).then(resolve, reject))
          : fetchAssets(src, scriptCache, fetch, false, loadError).then(resolve, reject);
      });
    } else if ((module && src) || ignore) {
      // module || ignore
      contentPromise = Promise.resolve('');
    } else if (!src) {
      // inline
      contentPromise = Promise.resolve(script.content);
    } else {
      // outline
      contentPromise = fetchAssets(src, scriptCache, fetch, false, loadError);
    }
    // refer https://html.spec.whatwg.org/multipage/scripting.html#attr-script-defer
    if (module && !async) script.defer = true;
    return { ...script, contentPromise };
  });
}

export function importHTML(params: { url: string; html?: string; opts: ImportEntryOpts }): Promise<htmlParseResult> {
  const { url, opts, html } = params;
  const fetch = opts.fetch ?? defaultFetch;
  const fiber = opts.fiber ?? true;
  const { plugins, loadError } = opts;
  const htmlLoader = plugins ? compose(plugins!.map((plugin) => plugin.htmlLoader!)) : defaultGetTemplate;
  const jsExcludes = getEffectLoaders('jsExcludes', plugins!);
  const cssExcludes = getEffectLoaders('cssExcludes', plugins!);
  const jsIgnores = getEffectLoaders('jsIgnores', plugins!);
  const cssIgnores = getEffectLoaders('cssIgnores', plugins!);
  const getPublicPath = defaultGetPublicPath;

  const getHtmlParseResult = (url: string, html: string, htmlLoader: any) => {
    return (
      html
        ? Promise.resolve(html)
        : fetch(url)
            .then((response) => {
              if (response.status >= 400) {
                log(`${LS_MICRO_TIPS_HTML_ERROR_REQUESTED}: ${JSON.stringify({ url, response })}`, 'error');
                loadError?.(url, new Error(LS_MICRO_TIPS_HTML_ERROR_REQUESTED));
                return '';
              }
              return response.text();
            })
            .catch((e) => {
              embedHTMLCache[url] = null;
              loadError?.(url, e);
              return Promise.reject(e);
            })
    ).then((html) => {
      const assetPublicPath = getPublicPath(url);
      const { template, scripts, styles } = processTpl(htmlLoader(html), assetPublicPath);
      return {
        template,
        assetPublicPath,
        getExternalScripts: () =>
          getExternalScripts(
            scripts
              .filter((script) => !script.src || !isMatchUrl(script.src, jsExcludes))
              .map((script) => ({ ...script, ignore: !!script.src && isMatchUrl(script.src, jsIgnores) })),
            fetch,
            loadError!,
            fiber,
          ),
        getExternalStyleSheets: () =>
          getExternalStyleSheets(
            styles
              .filter((style) => !style.src || !isMatchUrl(style.src, cssExcludes))
              .map((style) => ({ ...style, ignore: !!style.src && isMatchUrl(style.src, cssIgnores) })),
            fetch,
            loadError!,
          ),
      };
    });
  };

  if (opts?.plugins?.some((plugin) => plugin.htmlLoader)) {
    return getHtmlParseResult(url, html!, htmlLoader);
  } else {
    // 没有html-loader可以做缓存
    // eslint-disable-next-line no-return-assign
    return embedHTMLCache[url] || (embedHTMLCache[url] = getHtmlParseResult(url, html!, htmlLoader));
  }
}

/**
 * 处理css-loader
 */
export async function processCssLoader(
  sandbox: LSMICRO,
  template: string,
  getExternalStyleSheets: () => StyleResultList,
): Promise<string> {
  const curUrl = getCurUrl(sandbox.proxyLocation);
  /** css-loader */
  const composeCssLoader = compose(sandbox.plugins.map((plugin) => plugin.cssLoader));
  const processedCssList: StyleResultList = getExternalStyleSheets().map(({ src, ignore, contentPromise }) => ({
    src,
    ignore,
    contentPromise: contentPromise.then((content) => composeCssLoader(content, src, curUrl)),
  }));
  const embedHTML = await getEmbedHTML(template, processedCssList);
  return sandbox.replace ? sandbox.replace(embedHTML) : embedHTML;
}

/**
 * convert external css link to inline style for performance optimization
 * @return embedHTML
 */
async function getEmbedHTML(template: any, styleResultList: StyleResultList): Promise<string> {
  let embedHTML = template;

  return Promise.all(
    styleResultList.map((styleResult, index) =>
      styleResult.contentPromise.then((content) => {
        if (styleResult.src) {
          embedHTML = embedHTML.replace(
            genLinkReplaceSymbol(styleResult.src),
            styleResult.ignore
              ? `<link href="${styleResult.src}" rel="stylesheet" type="text/css">`
              : `<style>/* ${styleResult.src} */${content}</style>`,
          );
        } else if (content) {
          embedHTML = embedHTML.replace(
            getInlineStyleReplaceSymbol(index),
            `<style>/* inline-style-${index} */${content}</style>`,
          );
        }
      }),
    ),
  ).then(() => embedHTML);
}
