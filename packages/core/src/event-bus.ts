/**
 * @file 全局eventBus，用于不同框架间进行通讯
 */

import { LS_MICRO_ALL_EVENTS } from './constants';

export type EventObjType = Record<string, Function[]>;

// IS_IN_LS_MICRO: 子应用是否在micro-fe环境中
export const eventStore = window.__IS_IN_LS_MICRO
  ? window.__LS_MICRO.inject.appEventObjMap
  : new Map<string, EventObjType>();

export class EventBus {
  public id: string;
  public eventObj: EventObjType;

  public constructor(id: string) {
    this.id = id;
    this.clear();
    if (!eventStore.has(this.id)) {
      eventStore.set(this.id, {});
    }
    this.eventObj = eventStore.get(this.id)!;
  }

  /** 监听事件 */
  public on(event: string, fn: Function) {
    const cbs = this.eventObj[event];
    if (!cbs) {
      this.eventObj[event] = [fn];
      return this;
    }
    if (!cbs.includes(fn)) {
      this.eventObj[event].push(fn);
    }
    return this;
  }

  /** 监听一次 */
  public once(event: string, fn: Function) {
    const cb = (...args: any[]) => {
      // 监听一次的时候，重新构造一个函数进行包裹，内部使用off取消对cb的监听，并执行函数体
      this.off(event, cb);
      fn(...args);
    };
    this.on(event, cb);
  }

  /** 取消某个函数监听 */
  public off(event: string, fn: Function) {
    const cbs = this.eventObj[event];
    cbs.filter((cb) => cb !== fn);
    return this;
  }

  /** 任何emit都会导致监听函数触发，第一个参数为事件名，后续的参数为emit的参数 */
  public onAll(fn: (event: string, ...args: any[]) => any) {
    return this.on(LS_MICRO_ALL_EVENTS, fn);
  }

  /** 取消onAll函数监听 */
  public offAll(fn: (event: string, ...args: any[]) => any) {
    return this.off(LS_MICRO_ALL_EVENTS, fn);
  }

  /** 发送全局事件 */
  public emit(event: string, ...args: any[]) {
    let cbs: Function[] = [];
    let allCbs: Function[] = [];
    eventStore.forEach((eventObj) => {
      cbs = cbs.concat(eventObj[event] ?? []);
      // 每次emit都会将LS_MICRO_ALL_EVENTS标记的事件推入执行数组
      allCbs = allCbs.concat(eventObj[LS_MICRO_ALL_EVENTS] ?? []);
    });

    try {
      cbs.forEach((cb) => cb(event, ...args));
      allCbs.forEach((cb) => cb(event, ...args));
    } catch (error) {
      console.error('[micro-event]', error);
    }

    return this;
  }

  /** 清空所有监听事件 */
  public clear() {
    const eventObj = eventStore.get(this.id) ?? {};
    Object.keys(eventObj).forEach((key) => {
      Reflect.deleteProperty(eventObj, key);
    });
    return this;
  }
}
