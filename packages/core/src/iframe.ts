/**
 * @file iframe相关逻辑
 */

import type { appAddEventListenerOptions } from './common';
import {
  appDocumentAddEventListenerEvents,
  appDocumentOnEvents,
  appWindowAddEventListenerEvents,
  appWindowOnEvent,
  documentProxyProperties,
  mainAndAppAddEventListenerEvents,
  mainDocumentAddEventListenerEvents,
  rawAddEventListener,
  rawDocumentQuerySelector,
  rawRemoveEventListener,
  rawWindowAddEventListener,
  rawWindowRemoveEventListener,
  windowProxyProperties,
  windowRegWhiteList,
} from './common';
import { LS_MICRO_DATA_FLAG, LS_MICRO_TIPS_ERROR_REQUESTED } from './constants';
import { getJsLoader } from './plugin';
import type { LSMICRO } from './sandbox';
import { renderElementToContainer } from './shadow';
import { syncUrlToWindow } from './sync';
import type { ScriptObject } from './template';
import type { ScriptObjectLoader } from './types';
import {
  anchorElementGenerator,
  execHooks,
  fixElementCtrSrcOrHref,
  getAbsolutePath,
  getCurUrl,
  getTagFromScript,
  isConstructable,
  isMatchSyncQueryById,
  log,
  setAttrsToElement,
  setTagToScript,
} from './utils';

/**
 * 加载iframe替换子应用
 *
 * @param src 应用src
 * @param element 元素
 * @param degradeAttrs 需降级元素的属性obj
 */
export function renderIframeReplaceApp(src: string, element: HTMLElement, degradeAttrs: { [key: string]: any } = {}) {
  const iframe = document.createElement('iframe');
  const defaultStyle = 'width: 100%; height: 100%';
  setAttrsToElement(iframe, { ...degradeAttrs, src, style: [defaultStyle, degradeAttrs.style].join(';') });
  renderElementToContainer(iframe, element);
}

/**
 * 修复vue绑定事件e.timeStamp < attachedTimestamp 的情况(TODO理解不了)
 */
export function patchEventTimeStamp(targetWindow: Window, iframeWindow: Window) {
  Object.defineProperty(targetWindow.Event.prototype, 'timeStamp', {
    get: () => {
      return iframeWindow.document.createEvent('Event').timeStamp;
    },
  });
}

/**
 * 修复元素副作用
 *
 * @param element 元素
 * @param iframeWindow iframe contentWindow
 * @returns
 */
export function patchElementEffect(
  element: (HTMLElement | Node | ShadowRoot) & { _hasPatch?: boolean },
  iframeWindow: Window,
): void {
  const proxyLocation = iframeWindow.__LS_MICRO.proxyLocation as Location;
  if (element._hasPatch) return;

  // 添加几个属性
  Object.defineProperties(element, {
    baseURI: {
      configurable: true,
      get: () => proxyLocation.protocol + '//' + proxyLocation.host + proxyLocation.pathname,
      set: undefined,
    },
    ownerDocument: {
      configurable: true,
      get: () => iframeWindow.document,
    },
    _hasPatch: { get: () => true },
  });

  // 执行钩子函数
  execHooks(iframeWindow.__LS_MICRO!.plugins!, 'patchElementHook', element, iframeWindow);
}

/**
 * 恢复非根节点的监听事件
 */
export function recoverEventListeners(rootElement: Element | ChildNode, iframeWindow: Window) {
  const sandbox = iframeWindow.__LS_MICRO;
  const elementEventCacheMap: WeakMap<
    Node,
    Array<{ type: string; handler: EventListenerOrEventListenerObject; options: any }>
  > = new WeakMap();
  // 生成treeWalker对象 只显示元素节点，false代表不扩展实体引用节点例如 &nbsp;
  // @ts-ignore
  const ElementIterator = document.createTreeWalker(rootElement, NodeFilter.SHOW_ELEMENT, null, false);
  let nextElement = ElementIterator.currentNode;
  while (nextElement) {
    const elementListenerList = sandbox.elementEventCacheMap.get(nextElement);
    if (elementListenerList?.length) {
      elementEventCacheMap.set(nextElement, elementListenerList);
      elementListenerList.forEach((listener) => {
        nextElement.addEventListener(listener.type, listener.handler, listener.options);
      });
    }
    // 使用nextNode()方法遍历满足要求的节点
    nextElement = ElementIterator.nextNode() as HTMLElement;
  }
  sandbox.elementEventCacheMap = elementEventCacheMap;
}

/**
 * 恢复根节点的监听事件
 */
export function recoverDocumentListeners(
  oldRootElement: Element | ChildNode,
  newRootElement: Element | ChildNode,
  iframeWindow: Window,
) {
  const sandbox = iframeWindow.__LS_MICRO;
  const elementEventCacheMap: WeakMap<
    Node,
    Array<{ type: string; handler: EventListenerOrEventListenerObject; options: any }>
  > = new WeakMap();
  const elementListenerList = sandbox.elementEventCacheMap.get(oldRootElement);
  if (elementListenerList?.length) {
    elementEventCacheMap.set(newRootElement, elementListenerList);
    elementListenerList.forEach((listener) => {
      newRootElement.addEventListener(listener.type, listener.handler, listener.options);
    });
  }
  sandbox.elementEventCacheMap = elementEventCacheMap;
}

/**
 * iframe插入脚本
 *
 * @param scriptResult script请求结果
 * @param iframeWindow
 * @param rawElement 原始的脚本
 */
export function insertScriptToIframe(
  scriptResult: ScriptObject | ScriptObjectLoader,
  iframeWindow: Window,
  rawElement?: HTMLScriptElement,
) {
  const { src, module, content, crossorigin, crossoriginType, async, attrs, callback, onload } =
    scriptResult as ScriptObjectLoader;
  const scriptElement = iframeWindow.document.createElement('script');
  const nextScriptElement = iframeWindow.document.createElement('script');
  const { replace, plugins, proxyLocation } = iframeWindow.__LS_MICRO;
  const jsLoader = getJsLoader({ plugins: plugins!, replace: replace! });
  let code = jsLoader(content!, src, getCurUrl(proxyLocation!));
  // 添加属性
  attrs &&
    Object.keys(attrs)
      .filter((key) => !Object.keys(scriptResult).includes(key))
      .forEach((key) => scriptElement.setAttribute(key, String(attrs[key])));

  // 内联脚本
  if (content) {
    // patch location,bind用来改变脚本中的this指向
    if (!iframeWindow.__LS_MICRO.degrade && !module) {
      code = `(function(window, self, global, location) {
        ${code}
      }).bind(window.__LS_MICRO.proxy)(
        window.__LS_MICRO.proxy,
        window.__LS_MICRO.proxy,
        window.__LS_MICRO.proxy,
        window.__LS_MICRO.proxyLocation,
      )`;
    }
    // 解决 webpack publicPath 为 auto 无法加载资源的问题
    Object.defineProperty(scriptElement, 'src', { get: () => src || '' });
  } else {
    src && scriptElement.setAttribute('src', src);
    crossorigin && scriptElement.setAttribute('crossorigin', crossoriginType!);
  }
  module && scriptElement.setAttribute('type', 'module');
  scriptElement.textContent = code || '';
  nextScriptElement.textContent =
    'if(window.__LS_MICRO.execQueue && window.__LS_MICRO.execQueue.length){ window.__LS_MICRO.execQueue.shift()()}';

  const container = rawDocumentQuerySelector.call(iframeWindow.document, 'head');
  const execNextScript = () => !async && container!.appendChild(nextScriptElement);
  const afterExecScript = () => {
    onload?.();
    execNextScript();
  };

  // 错误情况处理
  if (/^<!DOCTYPE html/i.test(code)) {
    log(`${LS_MICRO_TIPS_ERROR_REQUESTED}: ${scriptResult}`, 'error');
    return execNextScript();
  }

  // 打标记
  if (rawElement) {
    setTagToScript(scriptElement, getTagFromScript(rawElement)!);
  }

  const isOutlineScript = !content && src;

  // 外联脚本执行后的处理
  if (isOutlineScript) {
    scriptElement.onload = afterExecScript;
    scriptElement.onerror = afterExecScript;
  }
  container!.appendChild(scriptElement);

  // 调用回调
  callback?.(iframeWindow);
  // 执行 hooks
  execHooks(plugins!, 'appendOrInsertElementHook', scriptElement, iframeWindow, rawElement);
  // 内联脚本执行后的处理
  !isOutlineScript && afterExecScript();
}

/**
 * 给iframe实例上添加若干属性
 */
function patchIframeVariable(iframeWindow: Window, micro: LSMICRO, appHostPath: string): void {
  iframeWindow.__LS_MICRO = micro;
  iframeWindow.__LS_MICRO_PUBLIC_PATH = appHostPath + '/';
  iframeWindow.$micro = micro.provide;
  iframeWindow.__LS_MICRO_RAW_WINDOW__ = iframeWindow;
}

/**
 * 停止iframe loading
 * 防止运行主应用js代码，会给子应用带去副作用
 */
function stopIframeLoading(iframeWindow: Window) {
  const oldDoc = iframeWindow.document;
  return new Promise<void>((rs) => {
    function loop() {
      // todo， 这里需要再理解一下，为啥用setTimeout(cb, 1)呢？
      setTimeout(() => {
        let newDoc = null;
        try {
          newDoc = iframeWindow.document;
        } catch (err) {
          newDoc = null;
        }
        // wait for document ready
        if (!newDoc || newDoc === oldDoc) {
          loop();
        } else {
          // 停止iframe运行
          iframeWindow.stop ? iframeWindow.stop() : iframeWindow.document.execCommand('Stop');
          rs();
        }
      }, 1);
    }
    loop();
  });
}

/**
 * 初始化base标签
 *
 * <base> 标签是 HTML 中的一个元素，用于指定页面中相对 URL 的基准路径。它对于定义文档中的所有相对链接的基础 URL 是非常有用的。
 * <base> 标签通常位于 <head> 元素内部，并且只能在文档中出现一次
 * 可配置属性如下：
 * href：指定基准 URL，它可以是相对 URL 或绝对 URL。文档中的所有相对 URL 将以这个基准 URL 作为参考进行解析。
 * target：可选属性，指定链接的默认目标窗口或框架。如果链接没有指定目标窗口，则会使用 <base> 标签中指定的目标。
 */
function initBase(iframeWindow: Window, url: string) {
  const iframeDocument = iframeWindow.document;
  const baseElement = iframeDocument.createElement('base');
  const iframeUrlElement = anchorElementGenerator(iframeWindow.location.href);
  const appUrlElement = anchorElementGenerator(url);
  // 这里设置基准路径为 href = 协议 + 域名 + 路径
  baseElement.setAttribute('href', appUrlElement.protocol + '//' + appUrlElement.host + iframeUrlElement.pathname);
  // iframe.head添加了base标签
  iframeDocument.head.appendChild(baseElement);
}

/**
 * 对iframe的history的pushState和replaceState进行修改
 * 将从location劫持后的数据修改回来，防止跨域错误
 * 同步路由到主应用
 *
 * @param iframeWindow
 * @param appHostPath 子应用的 host path
 * @param mainHostPath 主应用的 host path
 */
function patchIframeHistory(iframeWindow: Window, appHostPath: string, mainHostPath: string) {
  const history = iframeWindow.history;
  const rawHistoryPushState = history.pushState;
  const rawHistoryReplaceState = history.replaceState;
  // 修改history.pushState
  history.pushState = function (data: any, title: string, url?: string): void {
    const baseUrl =
      mainHostPath + iframeWindow.location.pathname + iframeWindow.location.search + iframeWindow.location.hash;
    const mainUrl = getAbsolutePath(url?.replace(appHostPath, ''), baseUrl);
    const ignoreFlag = url === undefined;

    // 调用原生pushState,this指向iframe的history
    rawHistoryPushState.call(history, data, title, ignoreFlag ? undefined : mainUrl);
    if (ignoreFlag) return;
    // 动态修改base基准地址
    updateBase(iframeWindow, appHostPath, mainHostPath);
    // 同步子应用路由到主应用
    syncUrlToWindow(iframeWindow);
  };

  // 修改history.replaceState
  history.replaceState = function (data: any, title: string, url?: string): void {
    const baseUrl =
      mainHostPath + iframeWindow.location.pathname + iframeWindow.location.search + iframeWindow.location.hash;
    const mainUrl = getAbsolutePath(url?.replace(appHostPath, ''), baseUrl);
    const ignoreFlag = url === undefined;

    // 这里用原生的replaceState其他和history.pushState修改一致
    rawHistoryReplaceState.call(history, data, title, ignoreFlag ? undefined : mainUrl);
    if (ignoreFlag) return;
    updateBase(iframeWindow, appHostPath, mainHostPath);
    syncUrlToWindow(iframeWindow);
  };
}

/**
 * 动态的修改iframe的base地址
 *
 * @param iframeWindow
 * @param appHostPath
 * @param mainHostPath
 */
function updateBase(iframeWindow: Window, appHostPath: string, mainHostPath: string) {
  const baseUrl = new URL(iframeWindow.location.href?.replace(mainHostPath, ''), appHostPath);
  const baseElement = rawDocumentQuerySelector.call(iframeWindow.document, 'base');
  if (baseElement) baseElement.setAttribute('href', appHostPath + baseUrl.pathname);
}

/**
 * 修改window对象的事件监听，只有路由事件采用iframe的事件
 */
function patchIframeEvents(iframeWindow: Window) {
  iframeWindow.addEventListener = function addEventListener<K extends keyof WindowEventMap>(
    type: K,
    listener: (this: Window, ev: WindowEventMap[K]) => any,
    options?: boolean | appAddEventListenerOptions,
  ) {
    // 运行 钩子函数
    execHooks(iframeWindow.__LS_MICRO.plugins, 'windowAddEventListenerHook', iframeWindow, type, listener, options);

    // todo 需要理解为啥需要targetWindow这个属性？
    // 满足子应用监听需要挂载到window上的事件 && targetWindow 存在
    if (appWindowAddEventListenerEvents.includes(type) || (typeof options === 'object' && options.targetWindow)) {
      const targetWindow = typeof options === 'object' && options.targetWindow ? options?.targetWindow : iframeWindow;
      // 调用原生addEventListener this指向targetWindow
      return rawWindowAddEventListener.call(targetWindow, type, listener as any, options);
    }

    // 在子应用嵌套场景使用window.window获取真实window（为啥要这么搞？
    rawWindowAddEventListener.call(window.__LS_MICRO_RAW_WINDOW__ || window, type, listener as any, options);
  };

  iframeWindow.removeEventListener = function removeEventListener<K extends keyof WindowEventMap>(
    type: K,
    listener: (this: Window, ev: WindowEventMap[K]) => any,
    options?: boolean | appAddEventListenerOptions,
  ) {
    // 运行插件钩子函数
    execHooks(iframeWindow.__LS_MICRO.plugins, 'windowRemoveEventListenerHook', iframeWindow, type, listener, options);

    if (appWindowAddEventListenerEvents.includes(type) || (typeof options === 'object' && options.targetWindow)) {
      const targetWindow = typeof options === 'object' && options.targetWindow ? options?.targetWindow : iframeWindow;
      return rawWindowRemoveEventListener.call(targetWindow, type, listener as any, options);
    }
    rawWindowRemoveEventListener.call(window.__LS_MICRO_RAW_WINDOW__ || window, type, listener as any, options);
  };
}

/**
 * 记录节点的监听事件(修改节点的addEventListener和removeEventListener)
 */
function recordEventListeners(iframeWindow: Window) {
  const sandbox = iframeWindow.__LS_MICRO;

  iframeWindow.Node.prototype.addEventListener = function (
    type: string,
    handler: EventListenerOrEventListenerObject,
    options?: boolean | AddEventListenerOptions,
  ): void {
    // 添加事件缓存
    const elementListenerList = sandbox.elementEventCacheMap.get(this);
    if (elementListenerList) {
      if (!elementListenerList.find((listener) => listener.handler === handler)) {
        elementListenerList.push({ type, handler, options });
      }
    } else sandbox.elementEventCacheMap.set(this, [{ type, handler, options }]);
    return rawAddEventListener.call(this, type, handler, options);
  };

  iframeWindow.Node.prototype.removeEventListener = function (
    type: string,
    handler: EventListenerOrEventListenerObject,
    options?: boolean | EventListenerOptions,
  ): void {
    // 清除缓存
    const elementListenerList = sandbox.elementEventCacheMap.get(this);
    if (elementListenerList) {
      const index = elementListenerList?.findIndex((ele) => ele.handler === handler);
      elementListenerList.splice(index, 1);
    }
    if (!elementListenerList?.length) {
      sandbox.elementEventCacheMap.delete(this);
    }
    return rawRemoveEventListener.call(this, type, handler, options);
  };
}

/**
 * 子应用前进后退，同步路由到主应用
 *
 * @param iframeWindow
 */
export function syncIframeUrlToWindow(iframeWindow: Window): void {
  // 主要是监听 hashchange 和 popstate 事件
  iframeWindow.addEventListener('hashchange', () => syncUrlToWindow(iframeWindow));
  iframeWindow.addEventListener('popstate', () => {
    syncUrlToWindow(iframeWindow);
  });
}

/**
 * 处理iframeWindow的副作用
 */
function patchWindowEffect(iframeWindow: Window) {
  /** 处理属性函数（用于从window上把属性复制到iframeWindow上 */
  const processWindowProperty = (key: any) => {
    const value = iframeWindow[key];
    try {
      if (typeof value === 'function' && !isConstructable(value)) {
        // @ts-ignore
        iframeWindow[key] = window[key].bind(window);
      } else {
        // @ts-ignore
        iframeWindow[key] = window[key];
      }
      return true;
    } catch (error) {
      log((error as Error).message, 'warn');
      return false;
    }
  };

  Object.getOwnPropertyNames(iframeWindow).forEach((key) => {
    // 对某些key做特殊处理
    if (key === 'getSelection') {
      Object.defineProperty(iframeWindow, key, {
        // 这个属性获取变到document上了
        get: () => iframeWindow.document[key],
      });
      return;
    }
    // 单独属性处理
    if (windowProxyProperties.includes(key)) {
      processWindowProperty(key);
      return;
    }

    // window属性白名单
    windowRegWhiteList.some((reg) => {
      if (reg.test(key) && key in iframeWindow.parent) {
        return processWindowProperty(key);
      }
      return false;
    });
  });

  // onEvent事件 set
  const windowOnEvents = Object.getOwnPropertyNames(window)
    .filter((e) => /^on/.test(e))
    .filter((e) => !appWindowOnEvent.includes(e));

  // 过滤出的事件走主应用的window
  windowOnEvents.forEach((e) => {
    const descriptor = Object.getOwnPropertyDescriptor(iframeWindow, e) || {
      enumerable: true,
      writable: true,
    };

    try {
      Object.defineProperty(iframeWindow, e, {
        enumerable: descriptor.enumerable,
        configurable: true,
        get() {
          return window[e as any];
        },
        set:
          descriptor.writable || descriptor.set
            ? (handler) => {
                window[e as any] = typeof handler === 'function' ? handler.bind(iframeWindow) : handler;
              }
            : undefined,
      });
    } catch (error) {
      log((error as Error).message, 'warn');
    }
  });

  // 运行插件钩子函数
  execHooks(iframeWindow.__LS_MICRO.plugins, 'windowPropertyOverride', iframeWindow);
}

/**
 * 补充document副作用
 */
function patchDocumentEffect(iframeWindow: Window) {
  const sandbox = iframeWindow.__LS_MICRO;
  /**
   * 处理 addEventListener和removeEventListener
   * 由于这个劫持导致 handler 的this发生改变，所以需要handler.bind(document)
   * 但是这样会导致removeEventListener无法正常工作，因为handler => handler.bind(document)
   * 这个地方保存callback = handler.bind(document) 方便removeEventListener
   */
  const handlerCallbackMap: WeakMap<EventListenerOrEventListenerObject, EventListenerOrEventListenerObject> =
    new WeakMap();
  const handlerTypeMap: WeakMap<EventListenerOrEventListenerObject, Array<string>> = new WeakMap();

  // 重写iframeWindow上的addEventListener和removeEventListener
  iframeWindow.Document.prototype.addEventListener = function (
    type: string,
    handler: EventListenerOrEventListenerObject,
    options?: boolean | AddEventListenerOptions,
  ) {
    if (!handler) return;
    let callback = handlerCallbackMap.get(handler);
    const typeList = handlerTypeMap.get(handler);

    // 设置 handlerCallbackMap
    if (!callback) {
      callback = typeof handler === 'function' ? handler.bind(this) : handler;
      handlerCallbackMap.set(handler, callback);
    }

    // 设置 handlerTypeMap
    if (typeList) {
      if (!typeList.includes(type)) typeList.push(type);
    } else {
      handlerTypeMap.set(handler, [type]);
    }

    // 运行插件钩子函数
    execHooks(iframeWindow.__LS_MICRO.plugins, 'documentAddEventListenerHook', iframeWindow, type, callback, options);

    if (appDocumentAddEventListenerEvents.includes(type)) {
      // 需要挂载到子应用iframe.document上的事件
      return rawAddEventListener.call(this, type, callback, options);
    }
    // 降级统一走 sandbox.document
    if (sandbox.degrade) return sandbox.document.addEventListener(type, callback, options);

    if (mainDocumentAddEventListenerEvents.includes(type)) {
      // 需要挂载到主应用document上的事件
      return window.document.addEventListener(type, callback, options);
    }

    if (mainAndAppAddEventListenerEvents.includes(type)) {
      // 需要同时挂载到window.document和shadowRoot上的事件
      window.document.addEventListener(type, callback, options);
      sandbox.shadowRoot.addEventListener(type, callback, options);
      return;
    }

    // 其余事件统一挂载到shadowRoot上
    sandbox.shadowRoot.addEventListener(type, callback, options);
  };

  iframeWindow.Document.prototype.removeEventListener = function (
    type: string,
    handler: EventListenerOrEventListenerObject,
    options?: boolean | AddEventListenerOptions,
  ) {
    const callback: EventListenerOrEventListenerObject = handlerCallbackMap.get(handler);
    const typeList = handlerTypeMap.get(handler);

    // callback不存在，直接退出
    if (!callback) return;

    if (typeList?.includes(type)) {
      // typeList删除这个type
      typeList.splice(typeList.indexOf(type), 1);
      if (!typeList.length) {
        handlerCallbackMap.delete(handler);
        handlerTypeMap.delete(handler);
      }
    }
    // 运行插件钩子函数
    execHooks(
      iframeWindow.__LS_MICRO.plugins,
      'documentRemoveEventListenerHook',
      iframeWindow,
      type,
      callback,
      options,
    );

    // 接下来逻辑与addEventListener一致，按type进行分类处理,调用不同对象removeEventListener
    if (appDocumentAddEventListenerEvents.includes(type)) {
      return rawRemoveEventListener.call(this, type, callback, options);
    }

    if (sandbox.degrade) return sandbox.document.removeEventListener(type, callback, options);

    if (mainDocumentAddEventListenerEvents.includes(type)) {
      return window.document.removeEventListener(type, callback, options);
    }
    if (mainAndAppAddEventListenerEvents.includes(type)) {
      window.document.removeEventListener(type, callback, options);
      sandbox.shadowRoot.removeEventListener(type, callback, options);
      return;
    }
    sandbox.shadowRoot.removeEventListener(type, callback, options);
  };

  // 处理onEvent
  const elementOnEvents = Object.keys(iframeWindow.HTMLElement.prototype).filter((ele) => /^on/.test(ele));
  const documentOnEvent = Object.keys(iframeWindow.Document.prototype)
    .filter((ele) => /^on/.test(ele))
    .filter((ele) => !appDocumentOnEvents.includes(ele));

  elementOnEvents
    .filter((e) => documentOnEvent.includes(e))
    .forEach((e) => {
      const descriptor = Object.getOwnPropertyDescriptor(iframeWindow.Document.prototype, e) || {
        enumerable: true,
        writable: true,
      };

      try {
        Object.defineProperty(iframeWindow.Document.prototype, e, {
          enumerable: descriptor.enumerable,
          configurable: true,
          // 降级的情况直接使用sandbox.document作为事件来源,否则使用shadowRoot
          // @ts-ignore
          get: () => (sandbox.degrade ? sandbox.document[e] : sandbox.shadowRoot.firstElementChild[e]),
          set:
            descriptor.writable || descriptor.set
              ? (handler) => {
                  const val = typeof handler === 'function' ? handler.bind(iframeWindow.document) : handler;
                  // @ts-ignore
                  sandbox.degrade ? (sandbox.document[e] = val) : (sandbox.shadowRoot.firstElementChild[e] = val);
                }
              : undefined,
        });
      } catch (e) {
        log((e as any).message, 'warn');
      }
    });

  // 处理属性get
  const {
    ownerProperties,
    modifyProperties,
    shadowProperties,
    shadowMethods,
    documentProperties,
    documentMethods,
    documentEvents,
  } = documentProxyProperties;

  modifyProperties.concat(shadowProperties, shadowMethods, documentProperties, documentMethods).forEach((propKey) => {
    const descriptor = Object.getOwnPropertyDescriptor(iframeWindow.Document.prototype, propKey) || {
      enumerable: true,
      writable: true,
    };
    try {
      Object.defineProperty(iframeWindow.Document.prototype, propKey, {
        enumerable: descriptor.enumerable,
        configurable: true,
        // @ts-ignore
        get: () => sandbox.proxyDocument[propKey],
        set: undefined,
      });
    } catch (e) {
      log((e as Error).message, 'warn');
    }
  });

  // 处理document专属事件
  // TODO 内存泄露
  documentEvents.forEach((propKey) => {
    const descriptor = Object.getOwnPropertyDescriptor(iframeWindow.Document.prototype, propKey) || {
      enumerable: true,
      writable: true,
    };
    try {
      Object.defineProperty(iframeWindow.Document.prototype, propKey, {
        enumerable: descriptor.enumerable,
        configurable: true,
        // @ts-ignore
        get: () => (sandbox.degrade ? sandbox : window).document[propKey],
        set:
          descriptor.writable || descriptor.set
            ? (handler) => {
                // @ts-ignore
                (sandbox.degrade ? sandbox : window).document[propKey] =
                  typeof handler === 'function' ? handler.bind(iframeWindow.document) : handler;
              }
            : undefined,
      });
    } catch (e) {
      log((e as Error).message, 'warn');
    }
  });

  // process owner property
  ownerProperties.forEach((propKey) => {
    Object.defineProperty(iframeWindow.document, propKey, {
      enumerable: true,
      configurable: true,
      // @ts-ignore
      get: () => sandbox.proxyDocument[propKey],
      set: undefined,
    });
  });

  // 运行插件钩子函数
  execHooks(iframeWindow.__LS_MICRO.plugins, 'documentPropertyOverride', iframeWindow);
}

/**
 * 处理node副作用
 *
 * 1、处理 getRootNode
 * 2、处理 appendChild、insertBefore，当插入的节点为 svg 时，createElement 的 patch 会被去除，需要重新 patch
 */
function patchNodeEffect(iframeWindow: Window) {
  const rawGetRootNode = iframeWindow.Node.prototype.getRootNode;
  const rawAppendChild = iframeWindow.Node.prototype.appendChild;
  const rawInsertRule = iframeWindow.Node.prototype.insertBefore;
  iframeWindow.Node.prototype.getRootNode = function (options?: GetRootNodeOptions): Node {
    const rootNode = rawGetRootNode.call(this, options);
    if (rootNode === iframeWindow.__LS_MICRO.shadowRoot) return iframeWindow.document;
    else return rootNode;
  };
  iframeWindow.Node.prototype.appendChild = function <T extends Node>(node: T): T {
    const res = rawAppendChild.call(this, node);
    patchElementEffect(node, iframeWindow);
    return res as T;
  };
  iframeWindow.Node.prototype.insertBefore = function <T extends Node>(node: T, child: Node | null): T {
    const res = rawInsertRule.call(this, node, child);
    patchElementEffect(node, iframeWindow);
    return res as T;
  };
}

/**
 * 补充相当路径的副作用
 */
function patchRelativeUrlEffect(iframeWindow: Window) {
  fixElementCtrSrcOrHref(iframeWindow, iframeWindow.HTMLImageElement, 'src');
  fixElementCtrSrcOrHref(iframeWindow, iframeWindow.HTMLAnchorElement, 'href');
  fixElementCtrSrcOrHref(iframeWindow, iframeWindow.HTMLSourceElement, 'src');
  fixElementCtrSrcOrHref(iframeWindow, iframeWindow.HTMLLinkElement, 'href');
  fixElementCtrSrcOrHref(iframeWindow, iframeWindow.HTMLScriptElement, 'src');
  fixElementCtrSrcOrHref(iframeWindow, iframeWindow.HTMLMediaElement, 'src');
}

/**
 * 初始化iframe dom结构
 */
// eslint-disable-next-line max-params
function initIframeDom(iframeWindow: Window, micro: LSMICRO, mainHostPath: string, appHostPath: string) {
  const iframeDocument = iframeWindow.document;
  // 创建新的document
  const newDoc = window.document.implementation.createHTMLDocument('');
  // 将新创建的document.documentElement拷贝一份后 添加到iframe的document上
  const newDocumentElement = iframeDocument.importNode(newDoc.documentElement, true);
  // iframe的document替换或添加新的documentElement
  iframeDocument.documentElement
    ? iframeDocument.replaceChild(newDocumentElement, iframeDocument.documentElement)
    : iframeDocument.appendChild(newDocumentElement);

  // 向iframe的window上补充各种属性
  iframeWindow.__LS_MICRO_RAW_DOCUMENT_HEAD__ = iframeDocument.head;
  iframeWindow.__LS_MICRO_RAW_DOCUMENT_QUERY_SELECTOR__ = iframeWindow.Document.prototype.querySelector;
  iframeWindow.__LS_MICRO_RAW_DOCUMENT_QUERY_SELECTOR_ALL__ = iframeWindow.Document.prototype.querySelectorAll;
  iframeWindow.__LS_MICRO_RAW_DOCUMENT_CREATE_ELEMENT__ = iframeWindow.Document.prototype.createElement;
  iframeWindow.__LS_MICRO_RAW_DOCUMENT_CREATE_TEXT_NODE__ = iframeWindow.Document.prototype.createTextNode;

  // 初始化base标签
  initBase(iframeWindow, micro.url);
  // 补充iframe history对象，进行若干修改
  patchIframeHistory(iframeWindow, appHostPath, mainHostPath);
  // 补充iframe event对象
  patchIframeEvents(iframeWindow);
  // 降级情况下，需要记录节点的监听事件？（todo 为啥？
  if (micro.degrade) recordEventListeners(iframeWindow);
  // 子应用前进后退，同步路由到主应用
  syncIframeUrlToWindow(iframeWindow);

  // 补充window副作用
  patchWindowEffect(iframeWindow);
  // 补充document副作用
  patchDocumentEffect(iframeWindow);
  // 处理node副作用
  patchNodeEffect(iframeWindow);
  // 处理相当路径副作用
  patchRelativeUrlEffect(iframeWindow);
}

/**
 * js沙箱
 * 创建和主应用 同源 的iframe，路径携带了子路由的路由信息
 * iframe必须禁止加载html，防止进入主应用的路由逻辑
 */
// eslint-disable-next-line max-params
export function iframeGenerator(
  sandbox: LSMICRO,
  attrs: { [key: string]: any },
  mainHostPath: string,
  appHostPath: string,
  appRoutePath: string,
) {
  const iframe = window.document.createElement('iframe');
  // src为主应用协议+域名的路径，默认display: none
  const attrsMerge = {
    src: mainHostPath,
    style: 'display: none',
    ...attrs,
    name: sandbox.id,
    [LS_MICRO_DATA_FLAG]: '',
  };
  // 给iframe设置属性
  setAttrsToElement(iframe, attrsMerge);
  // window.document appendChild上iframe
  window.document.body.appendChild(iframe);

  const iframeWindow = iframe.contentWindow;
  // 变量需要提前注入，在入口函数通过变量防止死循环
  patchIframeVariable(iframeWindow, sandbox, appHostPath);

  sandbox.iframeReady = stopIframeLoading(iframeWindow).then(() => {
    if (!iframeWindow.__LS_MICRO) {
      // 初始化，注入变量，这里估计是兜底，前面其实已经注入过了
      patchIframeVariable(iframeWindow, sandbox, appHostPath);
    }
    // 初始化iframe dom结构,进行各种逻辑处理
    initIframeDom(iframeWindow, sandbox, mainHostPath, appHostPath);
    // 如果有同步优先同步，非同步从url读取
    if (!isMatchSyncQueryById(iframeWindow.__LS_MICRO.id)) {
      iframeWindow.history.replaceState(null, '', mainHostPath + appRoutePath);
    }
  });

  return iframe;
}
