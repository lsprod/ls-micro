/**
 * @file 入口文件
 */

import { addSandboxCacheWithOptions, getMicroById, getOptionsById } from './common';
import { LS_MICRO_TIPS_NOT_SUPPORT, LS_MICRO_TIPS_NO_FETCH } from './constants';
import { importHTML, processCssLoader } from './entry';
import { EventBus } from './event-bus';
import { getPlugins } from './plugin';
import { LSMICRO } from './sandbox';
import { addLoading, defineLSWebComponent } from './shadow';
import { processSubAppHrefJump } from './sync';
import type { CacheOptions, PreOptions, StartOptions } from './types';
import { isMatchSyncQueryById, log, mergeOptions, microSupport, shutdownMainApp } from './utils';

/** 初始化LSMICRO */
function initLSMICRO() {
  // 全局eventBus实例
  const bus = new EventBus(Date.now().toString());

  // 中断主应用(有条件判断)
  shutdownMainApp();

  // 处理子应用链接跳转
  processSubAppHrefJump();

  // 自定义webComponent容器
  defineLSWebComponent();

  // 如果不支持则告警
  if (!microSupport) log(LS_MICRO_TIPS_NOT_SUPPORT, 'warn');

  // 查看是否有fetch
  if (!window.fetch) {
    log(LS_MICRO_TIPS_NO_FETCH, 'error');
    throw new Error();
  }

  return {
    bus,
  };
}

// 初始化必要配置
const initConfig = initLSMICRO();

/** 全局eventBus */
export const bus = initConfig.bus;

/**
 * 缓存子应用配置
 */
export function setupApp(options: CacheOptions) {
  if (options.name) addSandboxCacheWithOptions(options.name, options);
}

/**
 * 运行ls-micro
 */
export async function startApp(startOptions: StartOptions) {
  const sandbox = getMicroById(startOptions.name)!;
  const cacheOptions = getOptionsById(startOptions.name);
  // 合并缓存选项
  const options = mergeOptions(startOptions, cacheOptions!);
  const {
    name,
    url,
    html,
    replace,
    fetch,
    props,
    attrs,
    degradeAttrs,
    fiber,
    alive,
    degrade,
    sync,
    prefix,
    el,
    loading,
    plugins,
    lifecycles,
  } = options;

  if (sandbox) {
    // 已经初始化过的应用，快速渲染
    sandbox.plugins = getPlugins(plugins!);
    // @ts-ignore
    sandbox.lifecycle = lifecycles;
    const iframeWindow = sandbox.iframe!.contentWindow;
    if (sandbox.preload) {
      // 如果有预加载，则等待预加载完成
      await sandbox.preload;
    }

    if (alive) {
      // 保活
      await sandbox.active({ url: url!, sync, prefix, el, props, alive, fetch, replace });
      // 预加载但是没有执行的情况
      if (!sandbox.execFlag) {
        sandbox.lifecycle?.beforeLoad?.(sandbox.iframe!.contentWindow!);
        const { getExternalScripts } = await importHTML({
          url: url!,
          html,
          opts: {
            fetch: (fetch as typeof window.fetch) || window.fetch!,
            plugins: sandbox.plugins,
            loadError: sandbox.lifecycle!.loadError,
            fiber,
          },
        });
        // 启动子应用
        await sandbox.start(getExternalScripts);
      }
      // 触发activated钩子
      sandbox.lifecycle?.activated(sandbox.iframe!.contentWindow!);
      // 返回一个destroy方法，供子应用自行管理销毁逻辑
      return sandbox.destroy;
    } else if (iframeWindow.__LS_MICRO_MOUNT) {
      // 挂载函数存在于iframe的 contentWindow上
      /**
       * 子应用切换会触发webcomponent的disconnectedCallback调用sandbox.unmount进行实例销毁
       * 此处是防止没有销毁webcomponent时调用startApp的情况，需要手动调用unmount
       */
      sandbox.unmount();
      // 保活
      await sandbox.active({ url, sync, prefix, el, props, alive, fetch, replace });
      // 正常加载的情况，先注入css，最后才mount。重新激活也保持同样的时序
      sandbox.rebuildStyleSheets();
      // 走mount流程
      sandbox.lifecycle?.beforeMount?.(sandbox.iframe.contentWindow);
      iframeWindow.__LS_MICRO_MOUNT();
      sandbox.lifecycle?.afterMount?.(sandbox.iframe.contentWindow);
      // flag设为true
      sandbox.mountFlag = true;
      // 返回销毁函数
      return sandbox.destroy;
    } else {
      // 没有mount,直接销毁（好奇啥情况会走到这，可能是设置缘故
      sandbox.destroy();
    }
  }

  // 不存在实例时，重头开始渲染实例
  // 设置loading
  addLoading(el, loading);
  // 创建LSMICRO实例
  const newSandbox = new LSMICRO({ name, url, attrs, degradeAttrs, fiber, degrade, plugins, lifecycles });
  // 调用钩子函数
  newSandbox.lifecycle?.beforeLoad?.(newSandbox.iframe.contentWindow);
  const { template, getExternalScripts, getExternalStyleSheets } = await importHTML({
    url,
    html,
    opts: {
      fetch: (fetch as typeof window.fetch) || window.fetch,
      plugins: newSandbox.plugins,
      loadError: newSandbox.lifecycle.loadError,
      fiber,
    },
  });

  const processedHtml = await processCssLoader(newSandbox, template, getExternalStyleSheets);
  // active
  await newSandbox.active({ url, sync, prefix, template: processedHtml, el, props, alive, fetch, replace });
  // start
  await newSandbox.start(getExternalScripts);

  // 返回destroy
  return newSandbox.destroy;
}

/**
 * 预加载ls-micro
 */
export function preloadApp(preOptions: PreOptions): void {
  const requestIdleCallbackCb = () => {
    /**
     * 已经存在
     * url查询参数中有子应用的id，大概率是刷新浏览器或者分享url，此时需要直接打开子应用，无需预加载
     */
    if (getMicroById(preOptions.name) || isMatchSyncQueryById(preOptions.name)) return;
    const cacheOptions = getOptionsById(preOptions.name);
    // 合并缓存配置
    const options = mergeOptions({ ...preOptions }, cacheOptions);
    const {
      name,
      url,
      html,
      props,
      alive,
      replace,
      fetch,
      exec,
      attrs,
      degradeAttrs,
      fiber,
      degrade,
      prefix,
      plugins,
      lifecycles,
    } = options;
    const sandbox = new LSMICRO({ name, url, attrs, degradeAttrs, fiber, degrade, plugins, lifecycles });

    // 已经预加载过了，就直接返回
    if (sandbox.preload) return sandbox.preload;

    const runPreload = async () => {
      sandbox.lifecycle?.beforeLoad?.(sandbox.iframe.contentWindow);
      const { template, getExternalScripts, getExternalStyleSheets } = await importHTML({
        url,
        html,
        opts: {
          fetch: (fetch as typeof window.fetch) || window.fetch,
          plugins: sandbox.plugins,
          loadError: sandbox.lifecycle.loadError,
          fiber,
        },
      });
      const processedHtml = await processCssLoader(sandbox, template, getExternalStyleSheets);
      await sandbox.active({ url, props, prefix, alive, template: processedHtml, fetch, replace });
      if (exec) {
        await sandbox.start(getExternalScripts);
      } else {
        await getExternalScripts();
      }
    };
    sandbox.preload = runPreload();
  };

  requestIdleCallback(requestIdleCallbackCb);
}

/**
 * 销毁ls-micro APP
 */
export function destroyApp(id: string): void {
  const sandbox = getMicroById(id);
  if (sandbox) {
    sandbox.destroy();
  }
}

export { StartOptions, PreOptions, CacheOptions };
