/**
 * @file 插件相关逻辑
 */

import type { StyleObject } from './template';
import type { Plugin, ScriptObjectLoader } from './types';
import { compose, getAbsolutePath } from './utils';

interface loaderOption {
  plugins: Array<Plugin>;
  replace: (code: string) => string;
}

/**
 * 获取柯里化 cssLoader
 */
export function getCssLoader({ plugins, replace }: loaderOption) {
  return (code: string, src = '', base: string): string =>
    compose(plugins.map((plugin) => plugin.cssLoader!))(replace ? replace(code) : code, src, base);
}

/**
 * 获取柯里化 jsLoader
 */
export function getJsLoader({ plugins, replace }: loaderOption) {
  return (code: string, src = '', base: string): string =>
    compose(plugins.map((plugin) => plugin.jsLoader!))(replace ? replace(code) : code, src, base);
}

/**
 * 获取预置插件
 */
type presetLoadersType = 'cssBeforeLoaders' | 'cssAfterLoaders' | 'jsBeforeLoaders' | 'jsAfterLoaders';
export function getPresetLoaders(loaderType: presetLoadersType, plugins: Array<Plugin>): Plugin[presetLoadersType] {
  // @ts-ignore
  const loaders: (StyleObject | ScriptObjectLoader)[][] = plugins
    .map((plugin) => plugin[loaderType])
    .filter((loaders) => loaders?.length);
  const res = loaders.reduce((preLoaders, curLoaders) => preLoaders.concat(curLoaders), []);
  return loaderType === 'cssBeforeLoaders' ? res.reverse() : res;
}

/**
 * 获取影响插件
 */
type effectLoadersType = 'jsExcludes' | 'cssExcludes' | 'jsIgnores' | 'cssIgnores';
export function getEffectLoaders(loaderType: effectLoadersType, plugins: Array<Plugin>): Plugin[effectLoadersType] {
  return plugins
    .map((plugin) => plugin[loaderType])
    .filter((loaders) => loaders?.length)
    .reduce((preLoaders, curLoaders) => preLoaders!.concat(curLoaders!), []);
}

/**
 * 转换子应用css内的相对地址成绝对地址
 */
function cssRelativePathResolve(code: string, src: string, base: string) {
  const baseUrl = src ? getAbsolutePath(src, base) : base;
  // https://developer.mozilla.org/en-US/docs/Web/CSS/url
  const urlReg = /(url\((?!['"]?(?:data):)['"]?)([^'")]*)(['"]?\))/g;
  // eslint-disable-next-line max-params
  return code.replace(urlReg, (_m, pre, url, post) => {
    const absoluteUrl = getAbsolutePath(url, baseUrl);
    return pre + absoluteUrl + post;
  });
}

const defaultPlugin = {
  cssLoader: cssRelativePathResolve,
  // fix https://github.com/Tencent/wujie/issues/455
  cssBeforeLoaders: [{ content: 'html {view-transition-name: none;}' }],
};

/** 获取插件列表 */
export function getPlugins(plugins: Array<Plugin>): Array<Plugin> {
  return Array.isArray(plugins) ? [defaultPlugin, ...plugins] : [defaultPlugin];
}

/**
 * 判断url是否符合副作用loaders
 *
 * @param url url
 * @param effectLoaders 副作用loader
 */
export function isMatchUrl(url: string, effectLoaders: Plugin[effectLoadersType]): boolean {
  return effectLoaders!.some((loader) => (typeof loader === 'string' ? url === loader : loader.test(url)));
}
