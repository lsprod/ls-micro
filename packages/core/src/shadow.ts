/**
 * @file 容器相关逻辑封装（这里有点乱）
 */

import { getMicroById, rawAppendChild, rawElementAppendChild, relativeElementTagAttrMap } from './common';
import {
  CONTAINER_OVERFLOW_DATA_FLAG,
  CONTAINER_POSITION_DATA_FLAG,
  LOADING_DATA_FLAG,
  LS_MICRO_APP,
  LS_MICRO_APP_ID,
  LS_MICRO_IFRAME_CLASS,
  LS_MICRO_LOADING_STYLE,
  LS_MICRO_LOADING_SVG,
  LS_MICRO_SHADE_STYLE,
} from './constants';
import { patchRenderEffect } from './effect';
import { getExternalStyleSheets } from './entry';
import { patchElementEffect } from './iframe';
import { getCssLoader, getPresetLoaders } from './plugin';
import type { LSMICRO } from './sandbox';
import { clearChild, getAbsolutePath, getContainer, getCurUrl, setAttrsToElement } from './utils';

const cssSelectorMap = {
  ':root': ':host',
};

/**
 * 将准备好的内容插入容器
 *
 * @param element 需添加元素
 * @param selectorOrElement 父级容器
 * @return 容器dom引用
 */
export function renderElementToContainer(element: Element | ChildNode, selectorOrElement: string | HTMLElement) {
  const container = getContainer(selectorOrElement);
  if (container && !container.contains(element)) {
    // 有loading无需清理，已经清理过了
    if (!container.querySelector(`div[${LOADING_DATA_FLAG}]`)) {
      // 清除内容
      clearChild(container);
    }
    // 插入元素
    if (element) {
      rawElementAppendChild.call(container, element);
    }
  }
  return container;
}

export function createIframeContainer(id: string, degradeAttrs: { [key: string]: any } = {}): HTMLIFrameElement {
  const iframe = document.createElement('iframe');
  const defaultStyle = 'height:100%;width:100%';
  setAttrsToElement(iframe, {
    ...degradeAttrs,
    style: [defaultStyle, degradeAttrs.style].join(';'),
    // 指定子应用id
    [LS_MICRO_APP_ID]: id,
  });
  return iframe;
}

/**
 * 将降级的iframe挂在到容器上并进行初始化
 */
export function initRenderIframeAndContainer(
  id: string,
  parent: string | HTMLElement,
  degradeAttrs: { [key: string]: any } = {},
): { iframe: HTMLIFrameElement; container: HTMLElement } {
  const iframe = createIframeContainer(id, degradeAttrs);
  const container = renderElementToContainer(iframe, parent);
  const contentDocument = iframe.contentWindow!.document;
  contentDocument.open();
  contentDocument.write('<!DOCTYPE html><html><head></head><body></body></html>');
  contentDocument.close();
  return { iframe, container };
}

/**
 * 定义 ls webComponent，用shadowRoot包裹并获得dom装载和卸载的生命周期
 */
export function defineLSWebComponent() {
  const customElements = window.customElements;
  if (customElements && !customElements.get(LS_MICRO_APP)) {
    class LSMicroApp extends HTMLElement {
      // 自定义元素挂载时
      public connectedCallback() {
        if (this.shadowRoot) return;
        const shadowRoot = this.attachShadow({ mode: 'open' });
        const sandbox = getMicroById(this.getAttribute(LS_MICRO_APP_ID)!);
        patchElementEffect(shadowRoot, sandbox!.iframe!.contentWindow!);
        sandbox!.shadowRoot = shadowRoot;
      }

      // 自定义元素卸载时
      public disconnectedCallback() {
        const sandbox = getMicroById(this.getAttribute(LS_MICRO_APP_ID)!);
        sandbox?.unmount();
      }
    }
    // 定义自定义元素
    customElements?.define(LS_MICRO_APP, LSMicroApp);
  }
}

/**
 * 创建web component 容器
 */
export function createLSWebComponent(id: string): HTMLElement {
  const contentElement = window.document.createElement(LS_MICRO_APP);
  contentElement.setAttribute(LS_MICRO_APP_ID, id);
  contentElement.classList.add(LS_MICRO_IFRAME_CLASS);
  return contentElement;
}

/**
 * 将template渲染到iframe
 */
export async function renderTemplateToIframe(
  renderDocument: Document,
  iframeWindow: Window,
  template: string,
): Promise<void> {
  // 插入template
  const html = renderTemplateToHtml(iframeWindow, template);
  // 处理 css-before-loader 和 css-after-loader
  const processedHtml = await processCssLoaderForTemplate(iframeWindow.__LS_MICRO, html);
  renderDocument.replaceChild(processedHtml, renderDocument.documentElement);

  // 修复 html parentNode
  Object.defineProperty(renderDocument.documentElement, 'parentNode', {
    enumerable: true,
    configurable: true,
    get: () => iframeWindow.document,
  });

  patchRenderEffect(renderDocument, iframeWindow.__LS_MICRO.id!, true);
}

/**
 * 将template渲染成html
 */
export function renderTemplateToHtml(iframeWindow: Window, template: string) {
  const sandbox = iframeWindow.__LS_MICRO;
  const { head, body, alive, execFlag } = sandbox;
  const document = iframeWindow.document;

  let html = document.createElement('html');
  html.innerHTML = template;
  // 组件多次渲染，head和body必须一直使用同一个来应对被缓存的场景
  if (!alive && execFlag) {
    html = replaceHeadAndBody(html, head!, body!);
  } else {
    sandbox.head = html.querySelector('head')!;
    sandbox.body = html.querySelector('body')!;
  }
  // @ts-ignore
  const ElementIterator = document.createTreeWalker(html, NodeFilter.SHOW_ELEMENT, null, false);
  let nextElement = ElementIterator.currentNode as HTMLElement;
  while (nextElement) {
    patchElementEffect(nextElement, iframeWindow);
    const relativeAttr = relativeElementTagAttrMap[nextElement.tagName as keyof typeof relativeElementTagAttrMap];
    // @ts-ignore
    const url = nextElement[relativeAttr];
    if (relativeAttr) nextElement.setAttribute(relativeAttr, getAbsolutePath(url, nextElement.baseURI || ''));
    nextElement = ElementIterator.nextNode() as HTMLElement;
  }
  if (!html.querySelector('head')) {
    const head = document.createElement('head');
    html.appendChild(head);
  }
  if (!html.querySelector('body')) {
    const body = document.createElement('body');
    html.appendChild(body);
  }

  return html;
}

/**
 * 替换html中的head和body
 */
export function replaceHeadAndBody(html: HTMLHtmlElement, head: HTMLHeadElement, body: HTMLBodyElement) {
  const headElement = html.querySelector('head');
  const bodyElement = html.querySelector('body');
  if (headElement) {
    // 通过while进行遍历，将老的head中的元素依次添加到新的head中
    while (headElement.firstChild) {
      rawAppendChild.call(head, headElement.firstChild.cloneNode(true));
      headElement.removeChild(headElement.firstChild);
    }
    headElement.parentNode?.replaceChild(head, headElement);
  }
  if (bodyElement) {
    // body也是一样的操作
    while (bodyElement.firstChild) {
      rawAppendChild.call(body, bodyElement.firstChild.cloneNode(true));
      bodyElement.removeChild(bodyElement.firstChild);
    }
    bodyElement.parentNode?.replaceChild(body, bodyElement);
  }
  return html;
}

/**
 * 处理css-before-loader 以及 css-after-loader
 */
export async function processCssLoaderForTemplate(sandbox: LSMICRO, html: HTMLHtmlElement) {
  const document = sandbox.iframe!.contentDocument;
  const { plugins, replace, proxyLocation } = sandbox;
  const cssLoader = getCssLoader({ plugins: plugins!, replace: replace! });
  const cssBeforeLoaders = getPresetLoaders('cssBeforeLoaders', plugins!);
  const cssAfterLoaders = getPresetLoaders('cssAfterLoaders', plugins!);
  const curUrl = getCurUrl(proxyLocation!);

  const cssBeforeLoaderInitLogic = Promise.all(
    getExternalStyleSheets(cssBeforeLoaders!, sandbox.fetch, sandbox.lifecycle!.loadError).map(
      ({ src, contentPromise }) => contentPromise.then((content) => ({ src, content })),
    ),
  ).then((contentList) => {
    contentList.forEach(({ src, content }) => {
      if (!content) return;
      const styleElement = document!.createElement('style');
      styleElement.setAttribute('type', 'text/css');
      styleElement.appendChild(document!.createTextNode(content ? cssLoader(content, src, curUrl) : content));
      const head = html.querySelector('head');
      const body = html.querySelector('body');
      html.insertBefore(styleElement, head || body || html.firstChild);
    });
  });

  const cssAfterLoaderInitLogic = Promise.all(
    getExternalStyleSheets(cssAfterLoaders!, sandbox.fetch, sandbox.lifecycle!.loadError).map(
      ({ src, contentPromise }) => contentPromise.then((content) => ({ src, content })),
    ),
  ).then((contentList) => {
    contentList.forEach(({ src, content }) => {
      if (!content) return;
      const styleElement = document!.createElement('style');
      styleElement.setAttribute('type', 'text/css');
      styleElement.appendChild(document!.createTextNode(content ? cssLoader(content, src, curUrl) : content));
      html.appendChild(styleElement);
    });
  });

  return await Promise.all([cssBeforeLoaderInitLogic, cssAfterLoaderInitLogic]).then(
    () => html,
    () => html,
  );
}

/**
 * 获取修复好的样式元素
 * 主要是针对对root样式和font-face样式
 */
export function getPatchStyleElements(rootStyleSheets: Array<CSSStyleSheet>): Array<HTMLStyleElement | null> {
  const rootCssRules = [];
  const fontCssRules = [];
  const rootStyleReg = /:root/g;

  // 找出root的cssRules
  // eslint-disable-next-line @typescript-eslint/prefer-for-of
  for (let i = 0; i < rootStyleSheets.length; i++) {
    const cssRules: string | any[] | CSSRuleList = rootStyleSheets[i]?.cssRules ?? [];
    // eslint-disable-next-line @typescript-eslint/prefer-for-of
    for (let j = 0; j < cssRules.length; j++) {
      const cssRuleText = cssRules[j].cssText;
      // 如果是root的cssRule
      if (rootStyleReg.test(cssRuleText)) {
        rootCssRules.push(cssRuleText.replace(rootStyleReg, (match: any) => cssSelectorMap[match as ':root']));
      }
      // 如果是font-face的cssRule
      if (cssRules[j].type === CSSRule.FONT_FACE_RULE) {
        fontCssRules.push(cssRuleText);
      }
    }
  }

  let rootStyleSheetElement = null;
  let fontStyleSheetElement = null;

  // 复制到host上
  if (rootCssRules.length) {
    rootStyleSheetElement = window.document.createElement('style');
    rootStyleSheetElement.innerHTML = rootCssRules.join('');
  }

  if (fontCssRules.length) {
    fontStyleSheetElement = window.document.createElement('style');
    fontStyleSheetElement.innerHTML = fontCssRules.join('');
  }

  return [rootStyleSheetElement, fontStyleSheetElement];
}

/**
 * 将template渲染到shadowRoot
 */
export async function renderTemplateToShadowRoot(
  shadowRoot: ShadowRoot,
  iframeWindow: Window,
  template: string,
): Promise<void> {
  const html = renderTemplateToHtml(iframeWindow, template);
  // 处理 css-before-loader 和 css-after-loader
  const processedHtml = await processCssLoaderForTemplate(iframeWindow.__LS_MICRO, html);
  // change ownerDocument
  shadowRoot.appendChild(processedHtml);
  const shade = document.createElement('div');
  shade.setAttribute('style', LS_MICRO_SHADE_STYLE);
  processedHtml.insertBefore(shade, processedHtml.firstChild);
  shadowRoot.head = shadowRoot.querySelector('head')!;
  shadowRoot.body = shadowRoot.querySelector('body')!;

  // 修复 html parentNode
  Object.defineProperty(shadowRoot.firstChild, 'parentNode', {
    enumerable: true,
    configurable: true,
    get: () => iframeWindow.document,
  });

  patchRenderEffect(shadowRoot, iframeWindow.__LS_MICRO!.id!, false);
}

/**
 * 添加loading
 */
export function addLoading(el: HTMLElement | string, loading: HTMLElement) {
  // 获取容器
  const container = getContainer(el);
  // 清除容器内部节点
  clearChild(container);
  // 给容器设置一些样式，防止 loading 抖动
  let containerStyles = null;
  try {
    // 通过getComputedStyle获取动态样式
    containerStyles = window.getComputedStyle(container);
  } catch {
    return;
  }
  // 分position的情况分类讨论
  if (containerStyles.position === 'static') {
    container.setAttribute(CONTAINER_POSITION_DATA_FLAG, containerStyles.position);
    container.setAttribute(
      CONTAINER_OVERFLOW_DATA_FLAG,
      containerStyles.overflow === 'visible' ? '' : containerStyles.overflow,
    );
    container.style.setProperty('position', 'relative');
    container.style.setProperty('overflow', 'hidden');
  } else if (['relative', 'sticky'].includes(containerStyles.position)) {
    container.setAttribute(
      CONTAINER_OVERFLOW_DATA_FLAG,
      containerStyles.overflow === 'visible' ? '' : containerStyles.overflow,
    );
    container.style.setProperty('overflow', 'hidden');
  }
  // 创建一个div来呈现loading样式
  const loadingContainer = document.createElement('div');
  loadingContainer.setAttribute(LOADING_DATA_FLAG, '');
  loadingContainer.setAttribute('style', LS_MICRO_LOADING_STYLE);
  // 存在外部loadind html元素时，container加上这个loading
  if (loading) loadingContainer.appendChild(loading);
  // 否则用框架自带loading svg
  else loadingContainer.innerHTML = LS_MICRO_LOADING_SVG;

  // 容器appendChild上loadingContainer
  container.appendChild(loadingContainer);
}

/**
 * 移除loading
 */
export function removeLoading(el: HTMLElement): void {
  // 去除容器设置的样式
  const positionFlag = el.getAttribute(CONTAINER_POSITION_DATA_FLAG);
  const overflowFlag = el.getAttribute(CONTAINER_OVERFLOW_DATA_FLAG);
  if (positionFlag) el.style.removeProperty('position');
  if (overflowFlag !== null) {
    overflowFlag ? el.style.setProperty('overflow', overflowFlag) : el.style.removeProperty('overflow');
  }
  el.removeAttribute(CONTAINER_POSITION_DATA_FLAG);
  el.removeAttribute(CONTAINER_OVERFLOW_DATA_FLAG);
  const loadingContainer = el.querySelector(`div[${LOADING_DATA_FLAG}]`);
  loadingContainer && el.removeChild(loadingContainer);
}
