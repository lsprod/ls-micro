/**
 * @file 全局utils
 */

import { rawElementRemoveChild } from './common';
import { LS_MICRO_APP_ID, LS_MICRO_SCRIPT_ID, LS_MICRO_TIPS_NO_URL, LS_MICRO_TIPS_STOP_MAIN } from './constants';
import type { CacheOptions, Plugin } from './types';

/** 全局log, 可指定level */
export const log = (tips: string, level: 'log' | 'warn' | 'info' | 'error' = 'log') => {
  console[level](tips);
};

/** 是否支持微前端必备能力（proxy和自定义组件） */
export const microSupport = window.Proxy && window.CustomElementRegistry;

export function toArray<T>(array: T | T[]): T[] {
  return Array.isArray(array) ? array : [array];
}

export function isFunction(value: any): boolean {
  return typeof value === 'function';
}

/** 中断主应用内部逻辑 */
export function stopMainAppRun() {
  log(LS_MICRO_TIPS_STOP_MAIN);
  throw new Error(LS_MICRO_TIPS_STOP_MAIN);
}

/** 中断主应用 */
export function shutdownMainApp() {
  /**
   * 强行中断主应用
   * window.__LS_MICRO说明是主应用环境
   * window.__IS_IN_LS_MICRO如果为false说明子应用还没初始化完成
   */
  if (window.__LS_MICRO && !window.__IS_IN_LS_MICRO) {
    stopMainAppRun();
  }
}

/** 生成a标签 */
export function anchorElementGenerator(url: string) {
  const a = document.createElement('a');
  a.href = url;
  return a;
}

/** 获取a标签url的searchObj */
export function getAnchorElementQueryMap(a: HTMLAnchorElement) {
  const search = a.search.replace('?', '').split('&');
  return search.reduce(
    (map, item) => {
      const [key, value] = item.split('=');
      map[key] = value;
      return map;
    },
    // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
    {} as Record<string, string>,
  );
}

/** 获取元素容器 */
export function getContainer(container: string | HTMLElement): HTMLElement {
  return typeof container === 'string' ? (document.querySelector(container) as HTMLElement) : container;
}

/**
 * 清除Element所有节点
 */
export function clearChild(root: ShadowRoot | Node): void {
  // 循环清除第一个子元素
  while (root?.firstChild) {
    rawElementRemoveChild.call(root, root.firstChild);
  }
}

/** 设置元素attributes */
export function setAttrsToElement(element: HTMLElement, attrs: { [key: string]: any }) {
  Object.keys(attrs).forEach((name) => {
    element.setAttribute(name, attrs[name]);
  });
}

/** 父元素获取被降级的iframe元素 */
export function getDegradeIframe(id: string): HTMLIFrameElement {
  return window.document.querySelector(`iframe[${LS_MICRO_APP_ID}="${id}"]`)!;
}

/** 执行钩子函数 */
export function execHooks(plugins: Array<Plugin>, hookName: keyof Plugin, ...args: Array<any>): void {
  try {
    if (plugins && plugins.length > 0) {
      plugins
        .map((plugin) => plugin[hookName])
        .filter((hook) => isFunction(hook))
        .forEach((hook: any) => hook(...args));
    }
  } catch (e) {
    log(e as string, 'error');
  }
}

/** 合并缓存 */
// eslint-disable-next-line complexity
export function mergeOptions(options: CacheOptions, cacheOptions: CacheOptions) {
  return {
    name: options.name,
    el: options.el || cacheOptions?.el,
    url: options.url || cacheOptions?.url,
    html: options.html || cacheOptions?.html,
    exec: options.exec !== undefined ? options.exec : cacheOptions?.exec,
    replace: options.replace || cacheOptions?.replace,
    fetch: options.fetch || cacheOptions?.fetch,
    props: options.props || cacheOptions?.props,
    sync: options.sync !== undefined ? options.sync : cacheOptions?.sync,
    prefix: options.prefix || cacheOptions?.prefix,
    loading: options.loading || cacheOptions?.loading,
    // 默认 {}
    attrs: options.attrs !== undefined ? options.attrs : cacheOptions?.attrs || {},
    degradeAttrs: options.degradeAttrs !== undefined ? options.degradeAttrs : cacheOptions?.degradeAttrs || {},
    // 默认 true
    fiber: options.fiber !== undefined ? options.fiber : cacheOptions?.fiber !== undefined ? cacheOptions?.fiber : true,
    alive: options.alive !== undefined ? options.alive : cacheOptions?.alive,
    degrade: options.degrade !== undefined ? options.degrade : cacheOptions?.degrade,
    plugins: options.plugins || cacheOptions?.plugins,
    lifecycles: {
      beforeLoad: options.beforeLoad || cacheOptions?.beforeLoad,
      beforeMount: options.beforeMount || cacheOptions?.beforeMount,
      afterMount: options.afterMount || cacheOptions?.afterMount,
      beforeUnmount: options.beforeUnmount || cacheOptions?.beforeUnmount,
      afterUnmount: options.afterUnmount || cacheOptions?.afterUnmount,
      activated: options.activated || cacheOptions?.activated,
      deactivated: options.deactivated || cacheOptions?.deactivated,
      loadError: options.loadError || cacheOptions?.loadError,
    },
  };
}

/**
 * 获取绝对路径使用new URL处理
 *
 * @param url 拼接路径
 * @param base baseurl
 * @param hash 是否hash
 */
export function getAbsolutePath(url: string, base: string, hash?: boolean): string {
  try {
    // 为空值无需处理
    if (url) {
      // 需要处理hash的场景
      if (hash && url.startsWith('#')) return url;
      return new URL(url, base).href;
    } else return url;
  } catch {
    return url;
  }
}

/**
 * 获取需要同步的url
 */
export function getSyncUrl(id: string, prefix: { [key: string]: string }): string {
  let winUrlElement = anchorElementGenerator(window.location.href);
  const queryMap = getAnchorElementQueryMap(winUrlElement);
  // @ts-ignore
  winUrlElement = null;
  const syncUrl = window.decodeURIComponent(queryMap[id] || '');
  const validShortPath = syncUrl.match(/^{([^}]*)}/)?.[1];
  if (prefix && validShortPath) {
    return syncUrl.replace(`{${validShortPath}}`, prefix[validShortPath]);
  }
  return syncUrl;
}

/**
 * 将url解析为(protocol+host) (pathname+search+hash) (根据url生成的a标签)并返回
 *
 * @param url
 * @returns
 */
export function appRouteParse(url: string) {
  if (!url) {
    log(LS_MICRO_TIPS_NO_URL, 'error');
    throw new Error();
  }

  const urlElement = anchorElementGenerator(url);
  const appHostPath = urlElement.protocol + '//' + urlElement.host;
  let appRoutePath = urlElement.pathname + urlElement.search + urlElement.hash;
  return { urlElement, appHostPath, appRoutePath };
}

export function compose(fnList: Array<Function>): (...args: Array<string>) => string {
  return function (code: string, ...args: Array<any>) {
    return fnList.reduce((newCode, fn) => (isFunction(fn) ? fn(newCode, ...args) : newCode), code || '');
  };
}

/**
 * 劫持元素原型对相对地址的赋值转绝对地址
 */
export function fixElementCtrSrcOrHref(
  iframeWindow: Window,
  elementCtr:
    | typeof HTMLImageElement
    | typeof HTMLAnchorElement
    | typeof HTMLSourceElement
    | typeof HTMLLinkElement
    | typeof HTMLScriptElement
    | typeof HTMLMediaElement,
  attr: 'href' | 'src',
): void {
  // patch setAttribute
  const rawElementSetAttribute = iframeWindow.Element.prototype.setAttribute;
  elementCtr.prototype.setAttribute = function (name: string, value: string): void {
    let targetValue = value;
    // 对src和href转为绝对路径
    if (name === attr) targetValue = getAbsolutePath(value, this.baseURI || '', true);
    rawElementSetAttribute.call(this, name, targetValue);
  };
  // patch href get and set
  const rawAnchorElementHrefDescriptor = Object.getOwnPropertyDescriptor(elementCtr.prototype, attr);
  const { enumerable, configurable, get, set } = rawAnchorElementHrefDescriptor;
  Object.defineProperty(elementCtr.prototype, attr, {
    enumerable,
    configurable,
    get: function () {
      return get.call(this);
    },
    set: function (href) {
      set.call(this, getAbsolutePath(href, this.baseURI, true));
    },
  });
  // TODO: innerHTML的处理
}

/**
 * 当前url的查询参数中是否有给定的id
 */
export function isMatchSyncQueryById(id: string): boolean {
  const queryMap = getAnchorElementQueryMap(anchorElementGenerator(window.location.href));
  return Object.keys(queryMap).includes(id);
}

export function getCurUrl(proxyLocation: Object): string {
  const location = proxyLocation as Location;
  return location.protocol + '//' + location.host + location.pathname;
}

/** 获取内联内容 */
export function getInlineCode(match: string) {
  const start = match.indexOf('>') + 1;
  const end = match.lastIndexOf('<');
  return match.substring(start, end);
}

/** 是否劫持标签 */
export function isHijackingTag(tagName?: string) {
  return (
    tagName?.toUpperCase() === 'LINK' ||
    tagName?.toUpperCase() === 'STYLE' ||
    tagName?.toUpperCase() === 'SCRIPT' ||
    tagName?.toUpperCase() === 'IFRAME'
  );
}

/** 微任务回调 */
export function nextTick(cb: () => any): void {
  Promise.resolve().then(cb);
}

export function isScriptElement(element: HTMLElement): boolean {
  return element.tagName?.toUpperCase() === 'SCRIPT';
}

let count = 1;
export function setTagToScript(element: HTMLScriptElement, tag?: string): void {
  if (isScriptElement(element)) {
    const scriptTag = tag || String(count++);
    // element.setAttribute(WUJIE_SCRIPT_ID, scriptTag);
    element.setAttribute(LS_MICRO_SCRIPT_ID, scriptTag);
  }
}

export function getTagFromScript(element: HTMLScriptElement): string | null {
  if (isScriptElement(element)) {
    // return element.getAttribute(WUJIE_SCRIPT_ID);
    return element.getAttribute(LS_MICRO_SCRIPT_ID);
  }
  return null;
}

export function defaultGetPublicPath(entry: any) {
  if (typeof entry === 'object') {
    return '/';
  }
  try {
    const { origin, pathname } = new URL(entry, location.href);
    const paths = pathname.split('/');
    // 移除最后一个元素
    paths.pop();
    return `${origin}${paths.join('/')}/`;
  } catch (e) {
    console.warn(e);
    return '';
  }
}

/**
 * 事件触发器
 */
export function eventTrigger(el: HTMLElement | Window | Document, eventName: string, detail?: any) {
  let event;
  if (typeof window.CustomEvent === 'function') {
    event = new CustomEvent(eventName, { detail });
  } else {
    event = document.createEvent('CustomEvent');
    event.initCustomEvent(eventName, true, false, detail);
  }
  el.dispatchEvent(event);
}

const fnRegexCheckCacheMap = new WeakMap<any | FunctionConstructor, boolean>();
/** 是否是可构造函数 */
export function isConstructable(fn: () => any | FunctionConstructor) {
  const hasPrototypeMethods =
    fn.prototype && fn.prototype.constructor === fn && Object.getOwnPropertyNames(fn.prototype).length > 1;

  if (hasPrototypeMethods) return true;

  if (fnRegexCheckCacheMap.has(fn)) {
    return fnRegexCheckCacheMap.get(fn);
  }

  let constructable = hasPrototypeMethods;
  if (!constructable) {
    const fnString = fn.toString();
    const constructableFunctionRegex = /^function\b\s[A-Z].*/;
    const classRegex = /^class\b/;
    constructable = constructableFunctionRegex.test(fnString) || classRegex.test(fnString);
  }

  fnRegexCheckCacheMap.set(fn, constructable);
  return constructable;
}

/**
 * in safari,(离谱啊)
 * typeof document.all === 'undefined' // true
 * typeof document.all === 'function' // true
 * We need to discriminate safari for better performance
 */
const naughtySafari = typeof document.all === 'function' && typeof document.all === 'undefined';
const callableFnCacheMap = new WeakMap<CallableFunction, boolean>();
export const isCallable = (fn: any) => {
  if (callableFnCacheMap.has(fn)) {
    return true;
  }

  const callable = naughtySafari ? typeof fn === 'function' && typeof fn !== 'undefined' : typeof fn === 'function';
  if (callable) {
    callableFnCacheMap.set(fn, callable);
  }
  return callable;
};

const boundedMap = new WeakMap<CallableFunction, boolean>();
export function isBoundedFunction(fn: CallableFunction) {
  if (boundedMap.has(fn)) {
    return boundedMap.get(fn);
  }
  const bounded = fn.name.indexOf('bound ') === 0 && !fn.hasOwnProperty('prototype');
  boundedMap.set(fn, bounded);
  return bounded;
}

const setFnCacheMap = new WeakMap<CallableFunction, CallableFunction>();
export function checkProxyFunction(value: any) {
  if (isCallable(value) && !isBoundedFunction(value) && !isConstructable(value)) {
    if (!setFnCacheMap.has(value)) {
      setFnCacheMap.set(value, value);
    }
  }
}

export function getTargetValue(target: any, p: any): any {
  const value = target[p];
  if (setFnCacheMap.has(value)) {
    return setFnCacheMap.get(value);
  }
  if (isCallable(value) && !isBoundedFunction(value) && !isConstructable(value)) {
    const boundValue = Function.prototype.bind.call(value, target);
    setFnCacheMap.set(value, boundValue);

    // eslint-disable-next-line guard-for-in
    for (const key in value) {
      boundValue[key] = value[key];
    }
    if (value.hasOwnProperty('prototype') && !boundValue.hasOwnProperty('prototype')) {
      // https://github.com/kuitos/kuitos.github.io/issues/47
      Object.defineProperty(boundValue, 'prototype', { value: value.prototype, enumerable: false, writable: true });
    }
    return boundValue;
  }
  return value;
}
