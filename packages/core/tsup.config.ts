import { defineConfig } from 'tsup';

const isDEV = process.env.NODE_ENV === 'development';

export default defineConfig({
  entry: ['src/index.ts'],
  format: ['esm', 'cjs'],
  outDir: 'dist',
  shims: true,
  dts: true,
  minify: !isDEV,
  clean: true,
  watch: isDEV,
});
