import React from 'react';
import type { StartOptions } from '@lgfe/fe-micro-core';
import { bus, preloadApp, startApp, destroyApp, setupApp } from '@lgfe/fe-micro-core';

export type Props = {
  height: string;
  width: string;
} & StartOptions;

export default class LSMicroReact extends React.PureComponent<Props> {
  // @ts-ignore
  public static bus = bus;
  public static setupApp = setupApp;
  public static preloadApp = preloadApp;
  public static destroyApp = destroyApp;

  public state = {
    ref: React.createRef<HTMLDivElement>(),
  };

  public destroy: () => void = null;
  public startAppQueue = Promise.resolve();

  public startApp = async () => {
    try {
      const props = this.props;
      const { current: el } = this.state.ref;
      this.destroy = await startApp({
        ...props,
        el: el as HTMLElement,
      });
    } catch (error) {
      console.error(error);
    }
  };

  public componentDidMount() {
    this.startApp();
  }

  public componentDidUpdate(prevProps: Props) {
    if (this.props.name !== prevProps.name || this.props.url !== prevProps.url) {
      this.startApp();
    }
  }

  public render() {
    const { width, height } = this.props;
    const { ref } = this.state;
    return <div style={{ width, height }} ref={ref} />;
  }
}
