const path = require('path');

/**
 * @type {import('webpack').Configuration}
 */
module.exports = {
  mode: 'production',
  entry: './index.tsx',
  target: ['web', 'es5'],
  output: {
    publicPath: '/',
    path: path.join(__dirname, 'dist/lib'),
    filename: 'index.js',
    library: 'LSMicroReact',
    libraryExport: 'default',
    libraryTarget: 'umd',
    globalObject: 'self',
    umdNamedDefine: true,
  },
  externals: {
    react: 'React',
  },
  devtool: 'source-map',
  resolve: {
    extensions: ['.ts', '.tsx'],
  },
  module: {
    rules: [
      {
        test: /\.[jt]sx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
    ],
  },
};
