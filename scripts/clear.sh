#! /usr/bin/env sh

set -eo pipefail

# 指定要删除的文件夹路径
folder_path="$(pwd)"

# 确保指定的文件夹存在
if [ -d "$folder_path" ]; then
  # 进入文件夹
  cd "$folder_path"
  
  # 查找并删除所有的 node_modules 文件夹
  find . -name "node_modules" -type d -prune -exec rm -rf '{}' +
  
  echo "已删除所有的 node_modules 文件夹。"
else
  echo "指定的文件夹不存在。"
fi